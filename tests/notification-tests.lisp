;;;; -*- Mode: Lisp -*-

;;;; notification-tests.lisp
;;;;
;;;; Tests regarding floating point exception handling..
;;;;
;;;; See the file COPYING in the main folder for license and copyright
;;;; information.


(in-package "CL-LIA-TESTS")

(use-package "FIVEAM")

(def-suite notification-tests
           :in cl-lia-test-suite
           :description
           "Tests checking float point numbers basic features.")

(in-suite notification-tests)


(test notification-style
  (is-true (member (cl-lia:current-notification-style)
                   '(:recording :error :terminating)
                   :test #'eq))

  (is-true (typep cl-lia:default-notification-style
                  'cl-lia:notification-style))

  (is-true (typep (cl-lia:current-notification-style)
                  'cl-lia:notification-style))

  (is-true (eq cl-lia:default-notification-style :error))
  )


(test (change-notification-style
       :depends-on notification-style)
  
  ;; Non dynamic notification implementations.

  (when (or cl-lia:dynamic-notification-style
            (eq :recording (cl-lia:current-notification-style)))
    (finishes (cl-lia:set-notification-style :recording))
    (is-true (eq :recording (cl-lia:current-notification-style)))
    (finishes (cl-lia:set-notification-style
               cl-lia:default-notification-style)))

  (when (or cl-lia:dynamic-notification-style
            (eq :terminating (cl-lia:current-notification-style)))
    (finishes (cl-lia:set-notification-style :terminating))
    (is-true (eq :terminating (cl-lia:current-notification-style)))
    (finishes (cl-lia:set-notification-style
               cl-lia:default-notification-style)))

  (when (or cl-lia:dynamic-notification-style
            (eq :error (cl-lia:current-notification-style)))
    (finishes (cl-lia:set-notification-style :error))
    (is-true (eq :error (cl-lia:current-notification-style)))
    (finishes (cl-lia:set-notification-style
               cl-lia:default-notification-style)))

  (unless cl-lia:dynamic-notification-style
    (signals error
      (cl-lia:with-notification-style (:recording) 0))
    (finishes
      (cl-lia:with-notification-style (:recording :notification nil) 0))
    )

  ;; Dynamic notification implementations.

  (when cl-lia:dynamic-notification-style
    (finishes (cl-lia:set-notification-style :error))
    (is-true (eq :error (cl-lia:current-notification-style)))
    (finishes (cl-lia:set-notification-style
               cl-lia:default-notification-style))
    )

  (when cl-lia:dynamic-notification-style
    (finishes (cl-lia:set-notification-style :recording))
    (is-true (eq :recording (cl-lia:current-notification-style)))
    (finishes (cl-lia:set-notification-style
               cl-lia:default-notification-style))
    )

  (when cl-lia:dynamic-notification-style
    (finishes (cl-lia:set-notification-style :terminating))
    (is-true (eq :terminating (cl-lia:current-notification-style)))
    (finishes (cl-lia:set-notification-style
               cl-lia:default-notification-style))
    )
  )


(test (notify-function
       :depends-on (and notification-style
                        change-notification-style))

  (when (or cl-lia:dynamic-notification-style
            (eq :error ; cl-lia:default-notification-style
                (cl-lia:current-notification-style)))

    (finishes (cl-lia:restore-default-floating-point-environment))

    (is-true
     (and (member :overflow
                  (cl-lia:fpe-traps))
          (not (member :overflow
                       (cl-lia:fpe-accrued-notifications)))))
             

    (signals cl-lia:floating-point-overflow
      (cl-lia:notify 'cl-lia:floating-point-overflow
                     :cv 42))

    ;; :error implies :recording.

    (is-true
     (and (member :overflow (cl-lia:fpe-traps))
          (member :overflow (cl-lia:fpe-accrued-notifications)))))


  (when (or cl-lia:dynamic-notification-style
            (eq :recording
                (cl-lia:current-notification-style)))

    (finishes (cl-lia:restore-default-floating-point-environment))

    (is-true
     (and (member :division-by-zero
                  (cl-lia:fpe-traps))
          (not (member :division-by-zero
                       (cl-lia:fpe-accrued-notifications)))))

    (finishes
      (cl-lia:notify 'cl-lia:division-by-zero
                     :cv 42))

    (is-true
     (and (member :division-by-zero
                  (cl-lia:fpe-traps))
          (member :division-by-zero
                  (cl-lia:fpe-accrued-notifications))))
    )
  
  )


;;;; end of file -- notification-tests.lisp
