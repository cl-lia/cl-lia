;;;; -*- Mode: Lisp -*-

;;;; cl-lia-test-suite.lisp
;;;;
;;;; Tests regarding non computational queries.
;;;;
;;;; See the file COPYING in the main folder for license and copyright
;;;; information.


(in-package "CL-LIA-TESTS")

(use-package "FIVEAM")

(5am:def-suite cl-lia-test-suite
           :description
           "The CL LIA Test Suite.")


;;;; end of file -- cl-lia-test-suite.lisp
