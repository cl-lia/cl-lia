;;;; -*- Mode: Lisp -*-

;;;; cl-lia-impl-dep-pkg.lisp
;;;;
;;;; The CL-LIA Implementation Dependencies package.
;;;;
;;;; This package exports implementation dependent values and other things.
;;;; The package is not required, but it is used as "trampoline" for
;;;; an implementation binding.
;;;;
;;;; See the file COPYING for license and copyright information.


(defpackage "IT.UNIMIB.DISCO.MA.CL.LIA-IMPL-DEP" (:use "CL")
  (:nicknames "LIA-ID" "LIA-IMPL-DEP" "CL-LIA-IMPL-DEP")
  (:documentation "The CL-LIA Implementation Dependencies Package.

This package (re)exports symbols that are needed to implement the
`Language Independent Interface' using the underlying implementation.


Notes:

Do not use this package where and when you are using the standard CL-LIA
package, unless you are prepared to do extensive manual shadowing
and (re) exporting.
"
   )

  ;; Common functionalities.

  (:export
   "NOT-IMPLEMENTED"
   "FUNCTION-NOT-IMPLEMENTED"
   "VARIABLE-NOT-IMPLEMENTED"
   "CONSTANT-NOT-IMPLEMENTED"
   "RAISE-NOT-IMPL"
   )

  (:export
   "RESULT-F"
   )
  

  ;; Shadow lists.

  ;; Basic aritmetic operations.

  (:shadow "+" "*" "-" "/" "=" "/=")

  ;; Trigonometric functions.

  (:shadow "SIN" "COS" "TAN" ; ...
   )

  ;; Exponentials and logarithms.

  (:shadow "EXP" "EXPT" "LOG")


  ;; Export lists.

  ;; FIXNUMs and INTEGERs

  (:export
   "BOUNDED-FIXNUM"
   "MINIMUM-FIXNUM"
   "MOST-NEGATIVE-FIXNUM"
   "MAXIMUM-FIXNUM"
   "MOST-POSITIVE-FIXNUM"
   "HAS-INFINITY-FIXNUM" ; This is NIL
   )

  (:export
   "BOUNDED-INTEGER" ; This is NIL
   "HAS-INFINITY-INTEGER" ; This is NIL
   "MINIMUM-INTEGER" ; What should this be?
   "MAXIMUM-INTEGER" ; What should this be?
   )


  ;; FLOATs

  (:export
   "MAKE-FLOAT"
   "MAKE-SHORT-FLOAT"
   "MAKE-SINGLE-FLOAT"
   "MAKE-DOUBLE-FLOAT"
   "MAKE-LONG-FLOAT"
   )

  (:export
   "NAN"
   "S-NAN"
   "Q-NAN"
   )

  (:export
   "IS-NAN"
   "NANP"
   "IS-QUIET-NAN"
   "QUIET-NAN-P"
   "IS-SIGNALING-NAN"
   "SIGNALING-NAN-P"
   )

  (:export
   "SHORT-FLOAT-NEGATIVE-INFINITY"
   "SHORT-FLOAT-POSITIVE-INFINITY"
   "SINGLE-FLOAT-NEGATIVE-INFINITY"
   "SINGLE-FLOAT-POSITIVE-INFINITY"
   "DOUBLE-FLOAT-NEGATIVE-INFINITY"
   "DOUBLE-FLOAT-POSITIVE-INFINITY"
   "LONG-FLOAT-NEGATIVE-INFINITY"
   "LONG-FLOAT-POSITIVE-INFINITY"
   )

  (:export
   "DENORM-FLOAT-TYPE-P"
   "DENORM-SHORT-FLOAT-P"
   "DENORM-SINGLE-FLOAT-P"
   "DENORM-DOUBLE-FLOAT-P"
   "DENORM-LONG-FLOAT-P"
   )

  (:export
   "INFS"
   "INFF"
   "INFD"
   "INFL"
   "-INFS"
   "-INFF"
   "-INFD"
   "-INFL"
   )

  (:export
   "IS-INFINITY"
   "INFINITYP"
   )

  (:export
   "IS-ZERO"
   "IS-NEG-ZERO"
   )


  ;; FP Notification Style

  (:export
   "NOTIFICATION-STYLE"

   "DYNAMIC-NOTIFICATION-STYLE"
   
   "CURRENT-NOTIFICATION-STYLE"
   "SET-NOTIFICATION-STYLE"
   "WITH-NOTIFICATION-STYLE"

   "NOTIFY"
   )

  ;; FP Notifications and Environment

  (:export
   "FPE-NOTIFICATION"
   "FPE-NOTIFICATION-SET"
   "+FPE-ALL-NOTIFICATIONS+"
   "SUPPORTED-NOTIFICATION-P"
   "ALL-SUPPORTED-NOTIFICATIONS-P"
   "SOME-SUPPORTED-NOTIFICATIONS-P"
   "FPE-TEST-NOTIFICATIONS"
   "FPE-CHECK-NOTIFICATIONS"
   "FPE-CLEAR-NOTIFICATIONS"
   )

  (:export
   "FLOATING-POINT-ENVIRONMENT"
   
   "FPE-TRAPS"
   "FPE-ROUNDING-MODE"
   "FPE-CURRENT-NOTIFICATIONS"
   "FPE-ACCRUED-NOTIFICATIONS"
   "FPE-PRECISION"
   "FPE-FAST-MODE-P"

   "SET-FLOATING-POINT-ENVIRONMENT"
   "GET-FLOATING-POINT-ENVIRONMENT"

   "DEFAULT-FLOATING-POINT-ENVIRONMENT"
   "RESTORE-DEFAULT-FLOATING-POINT-ENVIRONMENT"

   "WITH-FLOATING-POINT-ENVIRONMENT"
   )

  (:export
   "ROUNDING-MODE"
   "ROUNDING-STYLE"

   "ROUNDING-STYLE-FLOAT"
   "ROUNDING-STYLE-SHORT-FLOAT"
   "ROUNDING-STYLE-SINGLE-FLOAT"
   "ROUNDING-STYLE-DOUBLE-FLOAT"
   "ROUNDING-STYLE-LONG-FLOAT"

   "DEFAULT-ROUNDING-MODE"
   "GET-ROUNDING-MODE"
   "SET-ROUNDING-MODE"

   "WITH-ROUNDING-MODE"

   "ROUND-TO-ZERO"
   "ROUND-TO-NEAR"
   "ROUND-UPWARD"
   "ROUND-DOWNWARD"

   ;; Impl dep only functions.

   "ROUND-ZERO-FN"
   "ROUND-NEAR-FN"
   "ROUND-UP-FN"
   "ROUND-DOWN-FN"
   )

  ;; Non computational environment queries

  (:export
   "IS-CDR-IEEE-754-CONFORMANT"
   "IS-CDR-IEEE-754-CONSTANTS-PROVIDING"
   "IS-CDR-IEEE-754-ENVIRONMENT-PROVIDING"
   "IS-CDR-IEEE-754-OPERATION-PROVIDING"

   "IS-CL-USING-CDR-IEEE-754"
   )

  ;; Numeric operations

  (:export
   "+" "*" "-" "/"
   "_+_" "_*_" "-_" "_-_" "/_" "_/_"

   "1+" "1-" "INCF" "DECF"

   "CONJUGATE"
   )

  (:export
   "+.<" "+.>"
   "*.<" "*.>"
   "-.<" "-.>"
   "/.<" "/.>"
   )

  ;; Exponential, Logarithms and Trigonometry Operations

  (:export
   "ASIN" "ACOS" "ATAN"

   "SIN" "COS" "TAN"

   "ASINH" "ACOSH" "ATANH"

   "SINH" "COSH" "TANH"
   )

  (:export
   "PHASE"

   "EXP" "EXPT" "CIS"
   )

  (:export
   "SQRT"
   "ISQRT"
   )

  (:export
   "SIGNUM"
   "FLOAT-SIGN"
   )

  (:export
   "LOG"
   )

  ;; Numeric Comparison and Predicates

  (:export
   "/=" "<" "<=" "=" ">=" ">"

   "EVENP" "ODDP"
   "PLUSP" "MINUSP"
   "ZEROP"
   "MIN" "MAX"
   )

  (:export
   "IS-NEG-ZERO"
   "NEG-ZERO-P"
   )

  (:export
   "IS-TINY"
   "TINY-P"
   )

  (:export
   "RESIDUE"
   )

  (:export
   "EXPONENT" "FRACTION"
   )

  (:export
   "SCALE-FLOAT"
   )
  
  )


;;;; end of file -- cl-lia-pkg.lisp
