;;;; -*- Mode: Lisp -*-

;;;; notification-style.lisp
;;;;
;;;; Defintions relating to the different notification styles.
;;;;
;;;; These abbreviations are used in the text.
;;;;
;;;; * NRI : notification by recording in indicators (i.e., think C).
;;;; * NACF: notification by alteration of control flow (i.e., think
;;;;         "exceptions")
;;;; * NTM : notification by termination (i.e., think "exit" with code).
;;;;
;;;; See the file COPYING for license and copyright information.


(in-package "CL-LIA")

#| Refactored in "notification-style-common.lisp"
(deftype arithmetic-notification-style ()
  'cl-lia-impl-dep:arithmetic-notification-style)
|#

(defconstant dynamic-notification-style
  cl-lia-impl-dep:dynamic-notification-style
  "Flag indicating whether the implementation supports dynamic notification styles.

This flag specifies if the CL-LIA implementation allows the user to
switch between different notification styles.")


(defconstant default-notification-style :error
  "The default implementation style for Common Lisp.")


(defun current-notification-style ()
  "Return the current notification style."
  (cl-lia-impl-dep:current-notification-style))


(defun set-notification-style (ns)
  "Set the current notification style.

This function always always terminates correctly if NS is the current
notification style.


Notes:

Not all implementations support switching the notification style.
Besides the LIA specification advises against dynamically switching
notification style at runtime; however, in CL a user should be granted
this power (and responsability).


See Also:

CURRENT-NOTIFICATION-STYLE.
"
  (declare (type notification-style ns))
  (cl-lia-impl-dep:set-notification-style ns))


(defmacro with-notification-style ((ns &key (notification t)) &body body)
  "Executes BODY with notification style NS.

The keyword parameter NOTIFICATION can be T, NIL, or :WARN.

According to the value of NOTIFICATION the macro behaves differently.

If NOTIFICATION is T, the macro generates a PROGRAM-ERROR if it cannot
dynamically change the notification style.

If NOTIFICATION is NIL, the macro computes BODY using the current
notification style if it cannot change the notification style.

If NOTIFICATION is :WARN, and the macro cannot dynamically change the
notification style, the macro computes BODY using the current
notification style, but it issues a runtime warning stating the fact.

In all cases, the notification style once the macro returns is the one
that was set before the macro started.
"
  `(cl-lia-impl-dep:with-notification-style (,ns
                                             :notification ,notification)
     ,@body))


;;;; This depends on the fp-environment.


;;; notify
;;;
;;; This is really a place-holder for ERROR.
;;;
;;; Notes:
;;; Come back to refine the notion of "accrued" and "current"
;;; notifications, alongside the memorization of the continuation
;;; value.

(defun notify (condition &rest arguments)
  (if (stringp condition)
      (notify 'simple-error
              :format-control condition
              :format-arguments arguments)

      (let ((lia-fpn (lia-exception-as-fp-notification condition)))
        (declare (type (or null fpe-notification) lia-fpn))
        
        
        (destructuring-bind (&key
                             traps
                             current-notifications
                             accrued-notifications
                             &allow-other-keys)
            (get-floating-point-environment)
    

          (ecase (current-notification-style)
            (:recording

             ;; This is the complicated case.
             (let ((cnt-val (or (getf :continuation-value arguments)
                                (getf :cv arguments)
                                (getf :cont-value arguments)))
                   )

               (cond ((and lia-fpn (member lia-fpn traps :test #'eq))
                      (set-floating-point-environment
                       :current-notifications (union current-notifications
                                                     (list lia-fpn)
                                                     :test #'eq)
                       :accrued-notifications (cons lia-fpn accrued-notifications)
                       )
                      cnt-val
                      )
                     (lia-fpn
                      (set-floating-point-environment
                       :accrued-notifications (cons lia-fpn accrued-notifications)
                       )
                      cnt-val
                      )
                     (t ; (null lia-fpn)
                      (apply #'error condition arguments)
                      )))
             )
          
            (:error ; This implies :recording.
             (set-floating-point-environment
              :current-notifications (union current-notifications
                                            (list lia-fpn)
                                            :test #'eq)
              :accrued-notifications (cons lia-fpn accrued-notifications)
              )
             (apply #'error condition arguments)
             )

            (:terminating
             (let ((acnd (apply #'make-condition condition arguments)))
               (format *error-output* "CL-LIA: terminating with condition ~S."
                       acnd)
               (abort acnd))
             )
            )))
      ))


;;; process-notification
;;;
;;; This macro implements the processing of the different notification
;;; styles. It is the real work horse, in the sense that all
;;; operations may wrap their inner works with it.
;;;
;;; Notes:
;;; Come back to refine the notion of "accrued" and "current"
;;; notifications, alongside the memorization of the continuation
;;; value.

(defmacro process-notification ((&optional (continuation-value q-nan))
                                &body expr)
  "Wraps CL-LIA notification processing around EXPR.

This macro implements the processing of the different notification
styles. It is the real work horse, in the sense that all
operations may wrap their inner works with it.  Essentially it
intercepts the conditions coming from the implementation and handles
them according to the active NTM, NRI or NACF style.

Notes:

This is an internal macro that is not part of the CL-LIA interface.
"
  (let ((cnt-val-var (gentemp "CONT-VAL" "CL-LIA"))
        (notify-style-var (gentemp "NOTIFY-STYLE-CURRENT" "CL-LIA"))
        )

    `(let ((,notify-style-var (current-notification-style))
           (,cnt-val-var ,continuation-value)
           )
       (ecase notify-style
         (:terminating
          (handler-case ,expr
            (arithmetic-error (ae)
              (format *error-output*
                      "CL-LIA: terminating with condition ~S." ae)
              (abort ae))
            ))

         ((:recording :error)
          ;; FTTB

          (handler-case ,expr
            ((and lia-fp-exception cl:arithmetic-error) (lia-ae)
              (process-lia-arithmetic-error lia-ae ,notify-style-var ,cnt-val-var))
              
            (arithmetic-error (ae)
              (process-cl-arithmetic-error ae ,notify-style-var ,cnt-val-var))
            ))
         ))
    ))


(defun process-cl-arithmetic-error (ae notify-style cont-value)
  (declare (type arithmetic-error ae))

  (destructuring-bind (&key
                       traps
                       current-notifications
                       accrued-notifications
                       &allow-other-keys)
      (get-floating-point-environment)

    (let* ((lia-error-condition (cl-exception-corresponding-lia-exception ae))
           (lia-fpn (lia-exception-as-fp-notification lia-error-condition))
           )
      (declare (type (or null fpe-notification) lia-fpn))
      
      (cond ((and lia-fpn (member lia-fpn traps :test #'eq))
             
             ;; This may pile up the accrued notifications.
             (set-floating-point-environment
              :current-notifications (union current-notifications
                                            (list lia-fpn)
                                            :test #'eq)
              :accrued-notifications (cons lia-fpn accrued-notifications)
              ))
            
            (lia-fpn
             (set-floating-point-environment
              :accrued-notifications (cons lia-fpn accrued-notifications)
              ))
            
            (t ; (null lia-fpn) - An aritmethic error that is outside
               ; the CL-LIA specification.
             (error ae)
             ))
                  
      ;; If I get here, I had a proper LIA Arithmetic Error at hand.
      
      (if (eq notify-style :error)
          (error (change-class ae ; The magic of CHANGE-CLASS.
                               (cl-exception-corresponding-lia-exception ae)
                               :cv cont-value))
          cont-value))
    ))


(defun process-lia-arithmetic-error (ae notify-style cont-value)
  (declare (type lia-fp-exception ae))

  (destructuring-bind (&key
                       traps
                       current-notifications
                       accrued-notifications
                       &allow-other-keys)
      (get-floating-point-environment)

    (let ((lia-fpn (lia-exception-as-fp-notification ae)))
      (declare (type (or null fpe-notification) lia-fpn))
      
      (cond ((and lia-fpn (member lia-fpn traps :test #'eq))
             
             ;; This may pile up the accrued notifications.
             (set-floating-point-environment
              :current-notifications (union current-notifications
                                            (list lia-fpn)
                                            :test #'eq)
              :accrued-notifications (cons lia-fpn accrued-notifications)
              ))
            
            (lia-fpn
             (set-floating-point-environment
              :accrued-notifications (cons lia-fpn accrued-notifications)
              ))
            
            (t ; (null lia-fpn); A LIA aritmethic error that is totally messed up.
             (error "CL-LIA: major LIA FP Condition error (condition ~S)." ae)
             ))
      
      ;; If I get here, I had a proper LIA Arithmetic Error at hand.
      ;; To be fully paranoid, I should ensure that CONT-VALUE and the
      ;; continuation value in the LIA error are the same.  For the
      ;; time being I override the continuation value with what I have
      ;; passed here.
      
      (if (eq notify-style :error)
          (progn
            (set-fp-exception-cont-value cont-value ae)
            (error ae))

          ;; Otherwise I just return the continuation value as I
          ;; already have recorder the exception.

          cont-value)
      )))

;;;; end of file -- notification-style.lisp
