;;;; -*- Mode: Lisp -*-

;;;; non-comp-queries-tests.lisp
;;;;
;;;; Tests regarding non computational queries.
;;;;
;;;; See the file COPYING in the main folder for license and copyright
;;;; information.


(in-package "CL-LIA-TESTS")

(use-package "FIVEAM")

(def-suite cl-lia-non-comp-queries
           :in cl-lia-test-suite
           :description
           "Tests checking non computational (i.e., environmental) features.")

(in-suite cl-lia-non-comp-queries)

(test ieee-754-conformance
  #+lispworks
  (is-false (cl-lia:is-cdr-ieee-754-conformant))
  )

(test ieee-754-constants-provision
  #+lispworks
  (is-true (cl-lia:is-cdr-ieee-754-constants-providing))
  )

(test ieee-754-environment-provision
  #+lispworks
  (is-false (cl-lia:is-cdr-ieee-754-environment-providing))
  )

(test is-cl-using-cdr-ieee-754
  #+lispworks
  (is-false (cl-lia:is-cl-using-cdr-ieee-754))
  )


;;;; end of file -- non-comp-queries-tests.lisp
