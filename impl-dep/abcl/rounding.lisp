;;;; -*- Mode: Lisp -*-

;;;; rounding.lisp
;;;;
;;;; Lispworks Rounding modes etc.
;;;; Unfortunately LW does not expose any of the necessary
;;;; functionality yet.
;;;;
;;;; See the file COPYING for license and copyright information.


(in-package "CL-LIA-IMPL-DEP")

#|
  (:export
   "ROUNDING-MODE"
   "GET-ROUNDING-MODE"
   "SET-ROUNDING-MODE"

   "WITH-ROUNDING-MODE"

   "ROUND-TO-ZERO"
   "ROUND-TO-NEAR"
   "ROUND-UPWARD"
   "ROUND-DOWNWARD"
   )
|#


;;; rounding-mode
;;; Taken from the "common" defitions.

(deftype rounding-mode () 'cl-lia:rounding-mode)


(defun default-rounding-mode ()
  :nearest
  )

(defun get-rounding-mode ()
  :nearest
  )



(defun set-rounding-mode (rm)
  (declare (type rounding-mode rm)
           (ignore rm))
  (raise-not-impl 'function-not-implemented
                  :name 'cl-lia:set-rounding-mode))


(defmacro with-rounding-mode ((rm) &body body)
  (declare (ignore rm body))
  `(raise-not-impl 'macro-not-implemented
                   :name 'cl-lia:with-rounding-mode))


(defmacro round-to-zero (&body body)
  (declare (ignore body))
  `(raise-not-impl 'macro-not-implemented
                   :name 'cl-lia:round-to-zero))


#|
(defmacro round-to-near (&body body)
  (declare (ignore body))
  `(raise-not-impl 'macro-not-implemented
                   :name 'cl-lia:round-to-near))
|#


(defmacro round-to-near (&body body)
  `(progn ,@body))


(defmacro round-upward (&body body)
  (declare (ignore body))
  `(raise-not-impl 'macro-not-implemented
                   :name 'cl-lia:round-upward))


(defmacro round-downward (&body body)
  (declare (ignore body))
  `(raise-not-impl 'macro-not-implemented
                   :name 'cl-lia:round-downward))


(defun round-up-fn (x)
  (declare (type float x) (ignorable x))
  (raise-not-impl 'function-not-implemented
                  :name 'round-up-fn))

(defun round-down-fn (x)
  (declare (type float x) (ignorable x))
  (raise-not-impl 'function-not-implemented
                  :name 'round-down-fn))


(defun round-near-fn (x)
  (declare (type float x))
  x)


(defun round-zero-fn (x)
  (declare (type float x) (ignorable x))
  (raise-not-impl 'function-not-implemented
                  :name 'round-zero-fn))


;;;; end of file -- rounding.lisp
