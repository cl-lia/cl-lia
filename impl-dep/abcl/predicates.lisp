;;;; -*- Mode: Lisp -*-

;;;; predicates.lisp
;;;;
;;;; Predicates on numbers for LW.
;;;;
;;;; It is expected that an implementation will eventually provide
;;;; much lower level definitions.
;;;;
;;;; See the file COPYING for license and copyright information.


(in-package "CL-LIA-IMPL-DEP")


(defun is-zero (n)
  "Return T if the argument N is 'zero'."
  (declare (type number n))
  (cl:zerop n))

#|
(defun is-neg-zero (n)
  "Return T if the argument N is negative 'zero'.

Notes:

In CL this is usually NIL and in LW in particular, but other
implementations may diverge."
  (declare (type number n) (ignore n))
  nil)
|#

(defun is-neg-zero (n)
  "Return T if the argument N is negative 'zero'.

Notes:

The use of DECODE-FLOAT seem to work for LW."
  (declare (type number n))
  (and (cl:floatp n)
       (cl:zerop n)
       (cl:minusp (nth-value 2 (cl:integer-decode-float n)))))




;;;; end of file -- predicates.lisp
