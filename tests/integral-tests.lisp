;;;; -*- Mode: Lisp -*-

;;;; integral-tests.lisp
;;;;
;;;; Tests regarding non computational queries.
;;;;
;;;; See the file COPYING in the main folder for license and copyright
;;;; information.


(in-package "CL-LIA-TESTS")

(use-package "FIVEAM")

(def-suite integral-tests
           :in cl-lia-test-suite
           :description
           "Tests checking non computational (i.e., environmental) features.")

(in-suite integral-tests)

 #+common-lisp ; Just to emphasize.
(test integral-constants
  (is-true cl-lia:bounded-fixnum)
  (is-true (boundp 'cl-lia:minimum-fixnum))
  (is-true (boundp 'cl-lia:maximum-fixnum))
  (is-false cl-lia:bounded-integer)
  (is-false cl-lia:has-infinity-fixnum)
  (is-false cl-lia:has-infinity-integer)

  (signals error cl-lia:maximum-integer)
  (signals error cl-lia:minimum-integer)
  )


;;;; end of file -- integral-tests.lisp
