;;;; -*- Mode: Lisp -*-

;;;; notification-style.lisp
;;;;
;;;; Lispworks defintions relating to the different notification styles.
;;;;
;;;; These abbreviations are used in the text.
;;;;
;;;; * NRI : notification by recording in indicators (i.e., think C).
;;;; * NACF: notification by alteration of control flow (i.e., think
;;;;         "exceptions")
;;;; * NTM : notification by termination (i.e., think "exit" with code).
;;;;
;;;; See the file COPYING for license and copyright information.


(in-package "CL-LIA-IMPL-DEP")


#| Now we take the "common" definition.
(deftype notification-style ()
  '(member :recording   ; NRI
           :error       ; NACF
           :terminating ; NTM
           ))
|#

(deftype notification-style ()
  'cl-lia:notification-style)


(defconstant +current-notification-style+ :error) ; Only sane one for LW 7.1 and 8.0.


(defconstant dynamic-notification-style nil)


(defun current-notification-style ()
  +current-notification-style+)



(defun set-notification-style (ns)
  (declare (type notification-style ns))
  (unless (eq ns +current-notification-style+)
    (error "CL-LIA implementation: LW cannot set the notification style ~
            to ~S."
           ns))
  +current-notification-style+)


(defmacro with-notification-style ((ns &key (notification t)) &body body)
  (let ((nsvar (gensym "NS-")))
    `(let ((,nsvar (current-notification-style)))
       (hcl:unwind-protect-blocking-interrupts-in-cleanups
           (progn
             (if dynamic-notification-style
                 (set-notification-style ,ns)
                 (ecase ,notification
                   (:warn
                    (warn "LW cannot dynamically change notification style to ~S.~@
                           The computation will be carried out using ~
                           notification style ~S."
                          ,ns
                          ,nsvar))
                   ((t)
                    (error 'cl-lia::lia-simple-error
                           :format-control
                           "LW cannot dynamically change notification style."))
                   ((nil)
                    ; Do nothing
                    )
                   ))
             ,@body)
         (set-notification-style ,nsvar)))))


;;;; end of file -- notification-style.lisp
