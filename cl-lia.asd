;;;; -*- Mode: Lisp -*-

;;;; cl-lia.asd
;;;; 
;;;; The CL-LIA ASDF system.
;;;;
;;;; See the file COPYING for license and copyright information.


(asdf:defsystem :cl-lia
  :description "The CL LIA library."
  
  :author "Marco Antoniotti"

  :license "BSD"

  :components ((:file "cl-lia-pkg")
               (:file "cl-lia-impl-dep-pkg")

               (:module "common"
                :pathname ""
                :components ((:file "cl-lia-common")
                             (:file "rounding-common")
                             (:file "fp-exception-handling-common"
                              :depends-on ("cl-lia-common"))
                             (:file "notification-style-common")
                             )
                )
               
               (:module "impl-dep"
                :depends-on ("cl-lia-pkg"
                             "cl-lia-impl-dep-pkg"
                             "common"
                             )
                :components ((:file "common-defs")
                             #+lispworks
                             (:module "lispworks"
                              :depends-on ("common-defs")
                              :components ((:file "non-comp-queries")
                                           (:file "integral")
                                           (:file "floats")
                                           (:file "notification-style")
                                           (:file "fp-exception-handling")
                                           (:file "fp-environment")
                                           (:file "rounding"
                                            :depends-on ("floats"))
                                           (:file "helper-wrap-fns"
                                            :depends-on ("rounding"))
					   (:file "predicates"
                                            :depends-on ("integral" "floats" "helper-wrap-fns"))
                                           (:file "arithmetic-operations"
                                            :depends-on ("predicates" "floats"))
                                           )
                              )
                             )
                )
               (:module "CL-LIA-interface"
                :pathname ""
                :depends-on ("common" "impl-dep")
                :components ((:file "non-comp-queries")
                             (:file "integral")
                             (:file "floats")
                             (:file "fp-exception-handling")
                             (:file "fp-environment")
                             (:file "rounding")
                             (:file "notification-style"
                              :depends-on ("fp-exception-handling"
                                           "fp-environment"
                                           "rounding"))
                             )
                )
              (:module "CL-LIA-numeric-operations"
                :pathname ""
                :depends-on ("CL-LIA-interface")
                :components ((:file "helper-wrap-fns")
			     (:file "predicates"
                              :depends-on ("helper-wrap-fns"))
                             (:file "arithmetic-operations"
                              :depends-on ("predicates"))
                             )
                )
               )
  )

;;;; end of file cl-lia.asd
