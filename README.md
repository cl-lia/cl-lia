# CL-LIA

Marco Antoniotti  
See file COPYING for copyright and licensing information.


## DESCRIPTION

**CL-LIA** is a first cut implementation of the "Common Lisp Language
Independent Arithmetic" (ISO/IEC Information technology – Language
independent arithmetic ISO/IEC-10967) standards.

See the file COPYING for licensing information


## A NOTE ON FORKING

Of course you are free to fork the project subject to the current
licensing scheme.  However, before you do so, I ask you to consider
plain old "cooperation" by asking me to become a developer.
It helps keeping the entropy level at an acceptable level.
