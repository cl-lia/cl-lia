;;;; -*- Mode: Lisp -*-

;;;; float.lisp
;;;;
;;;; ABCL defintions relating to floating point types.
;;;;
;;;; See the file COPYING for license and copyright information.


(in-package "CL-LIA-IMPL-DEP")


(defun make-float (bits &optional (float-type *read-default-float-format*))
  (declare (ignore bits float-type))
  (raise-not-impl 'function-not-implemented
                  :name 'make-float))


;;; NaNs
;;;
;;; 20210912: Reworked; according to Martin Simmons (20210603) LW
;;; produces only quiet NaNs.

(defvar nan (coerce sys::*single-float-nan* *read-default-float-format*)
  "A NaN as defined in Lispworks SYSTEM package.")


(define-symbol-macro s-nan
  (raise-not-impl 'variable-not-implemented
                  :name 's-nan))

(setf (documentation 's-nan 'variable)
      "Signaling NaN are not available in Lispworks.")


#|
(define-symbol-macro q-nan
  (raise-not-impl 'variable-not-implemented
                  :name 'q-nan))

(setf (documentation 's-nan 'variable)
      "Explicit quiet NaN are not available in Lispworks.")
|#

(define-symbol-macro q-nan nan)

(setf (documentation 'q-nan 'variable)
      "ABCL quiet NaNs are just NaNs.")


(defun is-nan (x)
  "Returns T whenever the argument X is 'not a number' (NaN)."
  (sys:float-nan-p x))


(defun nanp (x)
  "Returns T whenever the argument X is 'not a number' (NaN)."
  (is-nan x))


#|
(defun is-quiet-nan (x)
  "Lispworks cannot test whether a a NaN is quiet.

Notes:

The function could in principle return NIL for anything non NAN, but
that would beat the purpose of the specification."
  (declare (ignore x))
  (raise-not-impl 'function-not-implemented :name 'is-quiet-nan)
  )
|#


(defun is-quiet-nan (x)
  "In ABCL if is a NaN, it is quiet."

  (is-nan x))


(defun quiet-nan-p (x)
  "In Lispworks if is a NaN, it is quiet."

  (is-quiet-nan x))


#|
(defun is-signaling-nan (x)
  "Lispworks cannot test whether a a NaN is signaling.

Notes:

The function could in principle return NIL for anything non NAN, but
that would beat the purpose of the specification."
  (declare (ignore x))
  (raise-not-impl 'function-not-implemented :name 'is-signaling-nan))
|#


(defun is-signaling-nan (x)
  "Lispworks cannot test whether a a NaN is signaling.
Also, it does not appear to produce signalling NaNs.  Therefore this
function always returs NIL."
  (declare (ignore x))
  (warn "CL-LIA-IMPL-DEP: Lispworks does not produce signaling NaNs.")
  nil)


(defun signaling-nan-p (x)
  "Lispworks cannot test whether a a NaN is signaling.

Notes:

The function could in principle return NIL for anything non NAN, but
that would beat the purpose of the specification."
  (is-signaling-nan x))


;;; Infinities

(defconstant short-float-positive-infinity
  ext:single-float-positive-infinity)
(defconstant short-float-negative-infinity
  ext:single-float-negative-infinity)

(defconstant single-float-positive-infinity
  ext:single-float-positive-infinity)
(defconstant single-float-negative-infinity
  ext:single-float-negative-infinity)

(defconstant double-float-positive-infinity
  ext:double-float-positive-infinity)
(defconstant double-float-negative-infinity
  ext:double-float-negative-infinity)

(defconstant long-float-positive-infinity
  ext:double-float-positive-infinity)
(defconstant long-float-negative-infinity
  ext:double-float-negative-infinity)


(defconstant infs short-float-positive-infinity)
(defconstant -infs short-float-negative-infinity)

(defconstant inff single-float-positive-infinity)
(defconstant -inff single-float-negative-infinity)

(defconstant infd double-float-positive-infinity)
(defconstant -infd double-float-negative-infinity)

(defconstant infl long-float-positive-infinity)
(defconstant -infl long-float-negative-infinity)


(defun is-infinity (x)
  "The function returns T whenever X is an infinity.

Otherwise it returns NIL.

To clarify, the X must be a representation of an IEEE infinity.
"
  (and (not (is-nan x))
       (or (cl:= x infs)
           (cl:= x -infs)
           (cl:= x inff)
           (cl:= x -inff)
           (cl:= x infd)
           (cl:= x -infd)
           (cl:= x infl)
           (cl:= x -infl)
           )))


(defun infinityp (x)
  "The function returns T whenever X is an infinity.

Otherwise it returns NIL.

To clarify, the X must be a representation of an IEEE infinity.
"
  (is-infinity x))


(defun is-positive-infinity (x)
  (or (cl:= x infs)
      (cl:= x inff)
      (cl:= x infd)
      (cl:= x infl)))


(defun is-negative-infinity (x)
  (or (cl:= x -infs)
      (cl:= x -inff)
      (cl:= x -infd)
      (cl:= x -infl)))


(defun positive-infinity-p (x)
  (is-positive-infinity x))


(defun negative-infinity-p (x)
  (is-negative-infinity x))


;;; Other floating points "constants" according to LIA 1, 5.2.

(defconstant rf #.(float-radix 1.0)) ; It is the same for all floats in ABCL.

(defconstant r-short-float rf)
(defconstant r-single-float rf)
(defconstant r-double-float rf)
(defconstant r-long-float rf)


;;; Note that (float-precision 0.0) is 0 in ABCL.

(defconstant p-short-float #.(float-precision 1.0s0))
(defconstant p-single-float #.(float-precision 1.0f0))
(defconstant p-double-float #.(float-precision 1.0d0))
(defconstant p-long-float #.(float-precision 1.0l0))


;;; Use SYS::FLOAT-EXPONENT; ask the Lispworks guys if it is correct.
;;; Note that (sys::float-exponent 0.0) is 0 in LW 7.1.

(defconstant emax-short-float #.(sys::float-exponent most-positive-short-float))
(defconstant emax-single-float #.(sys::float-exponent most-positive-single-float))
(defconstant emax-double-float #.(sys::float-exponent most-positive-double-float))
(defconstant emax-long-float #.(sys::float-exponent most-positive-long-float))

(defconstant emin-short-float
  #.(min (sys::float-exponent short-float-epsilon)
         (sys::float-exponent short-float-negative-epsilon)))
(defconstant emin-single-float
  #.(min (sys::float-exponent single-float-epsilon)
         (sys::float-exponent single-float-negative-epsilon)))
(defconstant emin-double-float
  #.(min (sys::float-exponent double-float-epsilon)
         (sys::float-exponent double-float-negative-epsilon)))
(defconstant emin-long-float
  #.(min (sys::float-exponent long-float-epsilon)
         (sys::float-exponent long-float-negative-epsilon)))


(defconstant denorm-float-type-p nil) ; Ask the LW guys if it is correct.

(defconstant denorm-short-float-p denorm-float-type-p)
(defconstant denorm-single-float-p denorm-float-type-p)
(defconstant denorm-double-float-p denorm-float-type-p)
(defconstant denorm-long-float-p denorm-float-type-p)


(eval-when (:load-toplevel :execute)
  (unless (cl:>= rf 2)
    (warn "CL-LIA: implementation dependent constant RF should be >= 2."))
  (let ((bound (cl:* 2 (cl:max 1 (cl:ceiling (cl:log (cl:* 2.0 cl:pi) rf))))))
    (unless (and (cl:>= p-short-float bound)
                 (cl:>= p-single-float bound)
                 (cl:>= p-double-float bound)
                 (cl:>= p-long-float bound))
      (warn "CL-LIA: some implementation dependent float precision constant is below the LIA mandated bound...")))
  (unless denorm-float-type-p
    (warn "CL-LIA: CL implementation does not ensure denormalized floats."))
  )

;;;; end of file -- float.lisp
