;;;; -*- Mode: Lisp -*-

;;;; lispworks-common.lisp
;;;;
;;;; Lispworks common parameters and functions.
;;;;
;;;; See the file COPYING for license and copyright information.


(in-package "CL-LIA-IMPL-DEP")


;;; Debugging.

(defparameter *unimplemented-warnings-p* nil)


;;;; end of file -- lispworks-common.lisp
