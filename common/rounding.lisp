;;;; -*- Mode: Lisp -*-

;;;; rounding.lisp
;;;;
;;;; Rounding modes etc.
;;;;
;;;; See the file COPYING for license and copyright information.


(in-package "CL-LIA")

#|
  (:export
   "ROUNDING-MODE"
   "GET-ROUNDING-MODE"
   "SET-ROUNDING-MODE"

   "WITH-ROUNDING-MODE"

   "ROUND-TO-ZERO"
   "ROUND-TO-NEAR"
   "ROUND-UPWARD"
   "ROUND-DOWNWARD"
   )
|#


(deftype rounding-mode ()
  "The Rounding Mode Type.

The meaning of these (enumerated) values correspond to a direction of
floating point rounding.


Notes:

The keywords roughly correspond to the C Library [4] FLT_ROUNDS values
of:

-1 indeterminable.
 0 toward zero.
 1 toward nearest.
 2 toward positive infinity.
 3 toward negative infinity.

As per the C Library standard, Common Lisp implementations can extend
the type rounding-modes with other keywords representing
implementation dependent rounding modes.
"
  '(member :indeterminable
           :zero
           :nearest
           :positive-infinity
           :negative-infinity))


(defun get-rounding-mode ()
  "The function returns the current rounding mode.

 The value :indeterminate is returned if such rounding mode cannot be
determined.
"
  (cl-lia-impl-dep:get-rounding-mode))



(defun set-rounding-mode (rm)
  (declare (type rounding-mode rm))
  "The function sets the rounding mode.

If the setting of the rounding mode is successful, then roundingnode
is returned as result and success is T. Otherwise, the rounding mode
before the the call is returned with success NIL.
"
  (cl-lia-impl-dep:set-rounding-mode rm))




(defmacro with-rounding-mode ((rm) &body body)
  "The with-rounding-mode Macro.

The code in body is executed with a floating point environment where
the rounding mode is set to RM. The previous rounding mode is saved
before executing body and it is restored (as if using unwind-protect)
upon exit.


Exceptional Situations:

The macro with-rounding-mode performs a minimal code-walk of body and
if it finds some floating point operation which potentially may not
respect the rounding mode rm issues a warning. It is assumed that this
warning will be raised at macro-expansion time.


See Also:

round-to-zero, round-to-near, round-upward, round-downward.
"
  `(cl-lia-impl-dep:with-rounding-mode (,rm) ,@body))


(defmacro round-to-zero (&body body)
  "Evaluate BODY in an environment where the rounding mode is set :ZERO.

The rounding mode is reset to the one surrounding the macro call upon
returning or raising a condition (as in unwind-protect).


Exceptional Situations:

None
"
  `(cl-lia-impl-dep:round-to-zero ,@body))


(defmacro round-to-near (&body body)
  "Evaluate BODY in an environment where the rounding mode is set :NEAREST.

The rounding mode is reset to the one surrounding the macro call upon
returning or raising a condition (as in unwind-protect).


Exceptional Situations:

None
"
  `(cl-lia-impl-dep:round-to-near ,@body))


(defmacro round-upward (&body body)
  "Evaluate BODY in an environment where the rounding mode is set :POSITIVE-INFINITY.

The rounding mode is reset to the one surrounding the macro call upon
returning or raising a condition (as in unwind-protect).


Exceptional Situations:

None
"
  `(cl-lia-impl-dep:round-upward ,@body))


(defmacro round-downward (&body body)
  "Evaluate BODY in an environment where the rounding mode is set :NEGATIVE-INFINITY.

The rounding mode is reset to the one surrounding the macro call upon
returning or raising a condition (as in unwind-protect).


Exceptional Situations:

None
"
  `(cl-lia-impl-dep:round-downward ,@body))



;;;; end of file -- rounding.lisp
