;;;; -*- Mode: Lisp -*-

;;;; rounding-common.lisp
;;;;
;;;; Rounding modes etc.
;;;; Common definitions etc used by the implementation dependent and
;;;; the implementation proper code.
;;;;
;;;; See the file COPYING for license and copyright information.


(in-package "CL-LIA")

#|
  (:export
   "ROUNDING-MODE"
   "GET-ROUNDING-MODE"
   "SET-ROUNDING-MODE"

   "WITH-ROUNDING-MODE"

   "ROUND-TO-ZERO"
   "ROUND-TO-NEAR"
   "ROUND-UPWARD"
   "ROUND-DOWNWARD"
   )
|#


(deftype rounding-mode ()
  "The Rounding Mode Type.

The meaning of these (enumerated) values correspond to a direction of
floating point rounding.


Notes:

The keywords roughly correspond to the C Library [4] FLT_ROUNDS values
of:

-1 indeterminable.
 0 toward zero.
 1 toward nearest.
 2 toward positive infinity.
 3 toward negative infinity.

As per the C Library standard, Common Lisp implementations can extend
the type rounding-modes with other keywords representing
implementation dependent rounding modes.
"
  '(member :indeterminable
           :zero
           :nearest
           :positive-infinity
           :negative-infinity))


(deftype rounding-style ()
  "The Rounding Style Type.

This is a enumeration type referring to LIA 1 Appendix 8."

  '(member :nearest-ties-to-even
           :nearest
           :truncate
           :other))


;;;; end of file -- rounding-common.lisp
