;;;; -*- Mode: Lisp -*-

;;;; common-defs.lisp
;;;;
;;;; Common definitions for implementation dependent functionalities.
;;;;
;;;; See the file COPYING for license and copyright information.


(in-package "CL-LIA-IMPL-DEP")


(define-condition not-implemented (cell-error)
  ()
  (:documentation
   "The condition signaled for 'non implemented' parts of the CL-LIA specification.")
  (:report
   (lambda (nic stream)
     (format stream
             "CL-LIA implementation: ~A is not implemented (yet) in ~A ~A."
             (cell-error-name nic)
             (lisp-implementation-type)
             (lisp-implementation-version)
             )))
  )


(define-condition function-not-implemented (not-implemented) ())

(define-condition macro-not-implemented (function-not-implemented) ())

(define-condition variable-not-implemented (not-implemented) ())

(define-condition constant-not-implemented (variable-not-implemented) ())


(defun raise-not-impl (ni-condition-class &rest initargs)
  (apply #'error ni-condition-class initargs))



(defparameter *unimplemented-warnings-p* nil)

(defun warn-not-impl (warn-fmt-control &rest warn-fmt-args)
  (when *unimplemented-warnings-p*
    (warn (format nil
                  "CL-LIA-IMPL-DEP: ~?"
                  warn-fmt-control
                  warn-fmt-args))))


;;;; end of file -- common-defs.lisp
