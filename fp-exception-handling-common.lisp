;;;; -*- Mode: Lisp -*-

;;;; fp-exception-handling-common.lisp
;;;;
;;;; Common defintions relating to floating points exception handling
;;;; (cfr., 3.3).
;;;;
;;;; This file also contains the "mirror" fp condition hierarchy that
;;;; is needed to handle "continuation" values.
;;;;
;;;;
;;;; See the file COPYING for license and copyright information.


(in-package "CL-LIA")


(deftype fpe-notification ()
  "The FPE notifications possibly supported by an implementation."

  ;; Maybe a better name would be 'indicators'.

  '(member :division-by-zero :infinitary
           :inexact
           :invalid
           :underflow
           :overflow
           :absolute-precision-underflow
           ))


;;; Floating Points conditions mirror hierarchy.

(define-condition lia-fp-exception (lia-error)
  ((cv :initarg :continuation-value
       :initarg :cv
       :initarg :cont-value
       :reader fp-exception-cont-value
       :writer set-fp-exception-cont-value)
   )
  (:documentation
   "The Lia FP Exception.

A 'mixin' class that is used to provide a slot holding the
\"continuation value\" for the error handling machinery.
"
   )
  )


(define-condition floating-point-inexact
    (lia-fp-exception cl:floating-point-inexact)
  ()
  )

(define-condition floating-point-invalid-operation
    (lia-fp-exception cl:floating-point-invalid-operation)
  ()
  )

(define-condition floating-point-overflow
    (lia-fp-exception cl:floating-point-overflow)
  ()
  )

(define-condition floating-point-underflow
    (lia-fp-exception cl:floating-point-underflow)
  ()
  )

(define-condition floating-point-absolute-precision-underflow
    (lia-fp-exception floating-point-underflow)
  ()
  )

(define-condition division-by-zero
    (lia-fp-exception cl:division-by-zero)
  ()
  )

(define-condition infinitary
    (lia-fp-exception cl:division-by-zero)
  ()
  )


;;; Conformance.

;;; Appendix A.2 Infinitary notification relaxation.
;;; CL modulated.

(defconstant silent-infinitary-fixnum nil)
(defconstant silent-infinitary-integer nil)
(defconstant silent-infinitary-rational nil)

(defconstant invalid-infinitary-fixnum nil)
(defconstant invalid-infinitary-integer nil)
(defconstant invalid-infinitary-rational nil)

(defconstant silent-infinitary-short-float nil)
(defconstant silent-infinitary-single-float nil)
(defconstant silent-infinitary-double-float nil)
(defconstant silent-infinitary-long-float nil)

(defconstant silent-infinitary-float nil)

(defconstant silent-infinitary-real nil)


;;; Appendix A.3 Infinitary notification relaxation.
;;; CL modulated.

(defconstant silent-inexact-short-float nil)
(defconstant silent-inexact-single-float nil)
(defconstant silent-inexact-double-float nil)
(defconstant silent-inexact-long-float nil)

(defconstant silent-inexact-float nil)

(defconstant silent-inexact-real nil)




;;; Utilities for the CL-LIA implementation.

(defparameter +excp-kwd-conditions+
  ;; Not a constant because of fascist compilers around; you know who
  ;; you are!
  '((:inexact . floating-point-inexact)
    (:invalid . floating-point-invalid-operation)
    (:overflow . floating-point-overflow)
    (:underflow . floating-point-underflow)
    (:absolute-precision-underflow . floating-point-absolute-precision-underflow)
    (:infinitary . infinitary)
    (:division-by-zero . infinitary)

    (:division-by-zero . division-by-zero) ; Ok. These two are a wart.
    (:infinitary . division-by-zero)
    ))


(defun fp-notification-as-lia-exception (excp-kwd)
  (declare (type fpe-notification excp-kwd))
  (cdr (assoc excp-kwd +excp-kwd-conditions+ :test #'eq)))


(defun lia-exception-as-fp-notification (excp)
  (typecase excp
    (lia-fp-exception
     (car (rassoc (type-of excp) +excp-kwd-conditions+ :test #'eq)))
    (symbol
     (car (rassoc excp +excp-kwd-conditions+ :test #'eq)))))



(defparameter +cl-vs-lia-exception-classes+
  ;; Again, not a constant because of fascist compilers around; you know who
  ;; you are!
  '((cl:floating-point-inexact . floating-point-inexact)
    (cl:floating-point-invalid-operation . floating-point-invalid-operation)
    (cl:floating-point-overflow . floating-point-overflow)
    (cl:floating-point-underflow . floating-point-underflow)

    ;; These do not exists in package "CL".
    ;; (cl:floating-point-absolute-precision-underflow . floating-point-absolute-precision-underflow)
    ;; (cl:floating-point-infinitary . infinitary)

    (cl:division-by-zero . infinitary) ; Ok. These two are a wart.
    (cl:division-by-zero . division-by-zero)
    )
  )


(defun cl-exception-corresponding-lia-exception (cl-excp &optional (errorp nil))
  (declare (type (or symbol condition) cl-excp))
  (let* ((cl-excp (etypecase cl-excp
                    (symbol cl-excp)
                    (condition (class-name (class-of (the condition cl-excp))))))
         (lia-excp (assoc cl-excp +cl-vs-lia-exception-classes+ :test 'eq))
         )
    (cond (lia-excp (cdr lia-excp))
          (errorp (error "CL-LIA: cannot find a LIA condition class corresponding to ~S."
                         cl-excp))
          )))



;;;; end of file -- fp-exception-handling-common.lisp
