;;;; -*- Mode: Lisp -*-

;;;; helper-wrap-fns.lisp
;;;;
;;;; The LIA specs use "helper" and "wrap" functions to clarify and
;;;; compact its various defintions.  This file contains some versions
;;;; of these functions.
;;;;
;;;; See the file COPYING for license and copyright information.
;;;;
;;;; Notes:
;;;; ======
;;;;
;;;; 20211212 MA:
;;;; Switched all calls to ERROR to NOTIFY.


(in-package "CL-LIA")

;;;; Integer helper and wrap functions.

;;; result-i

(declaim (ftype (function (integer) integer) result-integer))

(declaim (ftype (function (fixnum) fixnum) result-fixnum))


(defun result-fixnum (x)
  (declare (type fixnum x))
  x)

(declaim (inline result-fixnum))


(defun result-integer (x)
  (declare (type integer x)) ; Let's not get into (and integer (not fixunum))
  x)

(declaim (inline result-integer))


(defun result-i (x)
  (declare (type integer x))
  (etypecase x
    (fixnum (result-fixnum (the fixnum x)))
    (integer (result-integer x))))


;;; wrap-i

(declaim (ftype (function (integer) integer) wrap-integer))

(declaim (ftype (function (fixnum) fixnum) wrap-fixnum))


(defun wrap-fixnum (x)
  (declare (type fixnum x))
  x)

(declaim (inline wrap-integer))


(defun wrap-integer (x)
  (declare (type integer x)) ; Let's not get into (and integer (not fixunum))
  x)

(declaim (inline wrap-fixnum))


(defun wrap-i (x)
  (declare (type integer x))
  (etypecase x
    (fixnum (wrap-fixnum (the fixnum x)))
    (integer (wrap-integer x))))


;;;; Float helper and wrap functions and macros.
;;;; The "result_f" helper functions must rely on the underlying
;;;; implementation for the rising of overflow, underflow and inexact
;;;; conditions.
;;;;
;;;; Hence the helper functions are conflated into a macro.

#|
(defun result-short-float (x round-direction)
  (declare (ignore round-direction))
  x)

(defun result-single-float (x round-direction)
  (declare (ignore round-direction))
  x)

(defun result-double-float (x round-direction)
  (declare (ignore round-direction))
  x)

(defun result-long-float (x round-direction)
  (declare (ignore round-direction))
  x)
|#


;;; result-f
;;; In the LIA 1 and LIA 2 specs this is a family of functions with
;;; ROUND-DIRECTION actually a rounding function.  The implementation
;;; follows the LIA 2 spec, which is somewhat simpler (no choice of
;;; INEXACT condition available).
;;;
;;; In CL this has to be a macro, in order to catch the errors.  It
;;; dispatches to the implementation, which may or may not be
;;; "correct".  Bottom line, the LIA specs are for implementations
;;; with full control on the floating point computations; which is
;;; simply not available at this level of portable layer.
;;;
;;; The logic of this macro is that the implementation will possibly
;;; generate one of the FP exceptions and that we need to intercept them
;;; to tag on the appropriate continuation value.  The LIA spec
;;; functions instead check the value at hand and then decide what
;;; exception to raise, if necessary.
;;;
;;; Notes:
;;;
;;; There is a wart in the macro below as the ROUND-DIRECTION is not
;;; one of the three rounding function expected by the spec.  The
;;; result should be ok, but this is something that should be
;;; revisited later.
;;;
;;; Again, this spec is not built for speed or space, but for
;;; correctness. The macro below WILL cause code bloat and it WILL be
;;; slow.  Talk to your vendor/implementor to provide you with a
;;; CL-LIA compliant spec.
;;;
;;; See Also:
;;;
;;; LIA 1, Section 5.2.5; LIA 2, Section 5.2.1.

#|
(defun result-f (float-form &optional (round-direction :nearest))
  (declare (ignore round-direction))
  (warn "CL-LIA: come back to fix RESULT-F by dispatching to CL-LIA-IMPL-DEP.")
  float-form) ; Add proper handling of notification styles.
|#


(defmacro result-f (float-form &optional
                               (round-direction ''round-near-fn)
                               (result-type ''double-float))
  `(multiple-value-bind (result sign)
       (cl-lia-impl-dep:result-f ,float-form
                                 ,round-direction
                                 ,result-type)

     (declare (type (or float condition) result))

     (etypecase result
       (float result)

       (cl:floating-point-overflow
        (let ((cont-val (choose-overflow-cont-value sign ,round-direction)))
          (error (make-condition 'floating-point-overflow
                                 :cv cont-val)
                 )))

       (floating-point-overflow (error result)) ; Assume the
                                                ; cont-value is fine here.


       (cl:floating-point-underflow
        ;; We "flush to zero" and forget about denormalized numebrs FTTB.
        ;; I could check that SIGN is 0.0
        (let ((cont-val (coerce 0.0s0 ,result-type)
                        #| (choose-underflow-cont-value sign ,round-direction)|#
                        ))
          (error (make-condition 'floating-point-underflow
                                 :cv cont-val))))

       (floating-point-underflow (error result))

       ;; Missing, FTTB, the three I's: infinitary, invalid and inexact.
       )
     ))


(defun choose-overflow-cont-value (sign round-dir)
  (ecase round-dir
    (:nearest
     (if (plusp sign) inff -inff))
    
    (:positive-infinity
     (if (plusp sign) inff most-negative-single-float))

    (:negative-infinity
     (if (plusp sign) most-positive-single-float -inff))

    (:zero
     (if (plusp sign) most-positive-single-float most-negative-single-float))

    (:indeterminable
     (if (plusp sign) inff -inff))
    ))


;;; no-result-f

(defun no-result-f (x &optional (operation 'identity))
  ;; OPERATION is unary.
  (cond ((is-quiet-nan x) q-nan)

        ((is-signaling-nan x)
         (notify 'floating-point-invalid-operation
                 :operation operation
                 :operands (list s-nan)))

        (t ; Other cases are as above.
         (notify 'floating-point-invalid-operation
                 :operation operation
                 :operands (list x))
         ))
  )


;;; no-result-f-2

(defun no-result-f-2 (x y &optional (operation 'identity))
  ;; OPERATION is binary.
  (cond ((and (or (is-quiet-nan x) (is-quiet-nan y))
              (not (or (is-signaling-nan x) (is-signaling-nan y))))
         q-nan)
        ((or (is-signaling-nan x) (is-signaling-nan y))
         (notify 'floating-point-invalid-operation
                 :operation operation
                 :operands (list x y)))
        (t ; Other cases are as above.
         (notify 'floating-point-invalid-operation
                 :operation operation
                 :operands (list x y))
         ))
  )


;;;; end of file -- helper-wrap-fns.lisp
