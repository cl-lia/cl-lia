;;;; -*- Mode: Lisp -*-

;;;; arithmetic-operations.lisp
;;;;
;;;; Lispworks Arithmetic operations (cfr., 4.1)
;;;;
;;;;
;;;; See the file COPYING for license and copyright information.
;;;;
;;;; Notes:
;;;; 20211220 MA: I should set up the "Poor Man Templating" macrology.


(in-package "CL-LIA-IMPL-DEP")

#|

  (:export
   "+" "*" "-" "/"

   "1+" "1-" "INCF" "DECF"

   "CONJUGATE"
   )

  (:export
   "+.<" "+.>"
   "*.<" "*.>"
   "-.<" "-.>"
   "/.<" "/.>"
   )
|#

;;; Comparisons.

(defun _=_ (n1 n2)
  (cl:= n1 n2))

(defun _/=_ (n1 n2)
  (cl:/= n1 n2))

(defun _<_ (n1 n2)
  (cl:< n1 n2))

(defun _<=_ (n1 n2)
  (cl:<= n1 n2))

(defun _>_ (n1 n2)
  (cl:> n1 n2))

(defun _>=_ (n1 n2)
  (cl:>= n1 n2))


;;; Basic operations with rounding versions

(defun _+_ (n1 n2)
  (cl:+ n1 n2))

(defun _+.<_ (n1 n2)
  (cl:+ n1 n2))

(defun _+.>_ (n1 n2)
  (cl:+ n1 n2))

(defun _+.<>_ (n1 n2)
  (cl:+ n1 n2))


(defun _-_ (n1 n2)
  (cl:- n1 n2))

(defun _-.<_ (n1 n2)
  (cl:- n1 n2))

(defun _-.>_ (n1 n2)
  (cl:- n1 n2))

(defun _-.<>_ (n1 n2)
  (cl:- n1 n2))


(defun -_ (n1)
  (cl:- n1))


(defun _*_ (n1 n2)
  (cl:* n1 n2))

(defun _*.<_ (n1 n2)
  (cl:* n1 n2))

(defun _*.>_ (n1 n2)
  (cl:* n1 n2))

(defun _*.<>_ (n1 n2)
  (cl:* n1 n2))


(defun _/_ (n1 n2)
  (cl:/ n1 n2))

(defun _/.<_ (n1 n2)
  (cl:/ n1 n2))

(defun _/.>_ (n1 n2)
  (cl:/ n1 n2))

(defun _/.<>_ (n1 n2)
  (cl:/ n1 n2))


(defun /_ (n1 n2)
  (cl:/ n1 n2))

;;;; end of file -- arithmetic-operations.lisp
