;;;; -*- Mode: Lisp -*-

;;;; integral.lisp
;;;;
;;;; Defintions relating to integral types.
;;;;
;;;; See the file COPYING for license and copyright information.


(in-package "CL-LIA")


(defconstant bounded-fixnum t
  "The value of this constant is T.

This constant indicates that the Common Lisp datatype fixnum is bounded (with bounds
most-positive-fixnum and most-negative-fixnum).


Notes:

This is the parameter bounded_I with I equal to fixnum, required in LIA1.


See Also:

bounded-integer, minimum-fixnum, most-negative-fixnum,
maximum-fixnum, most-positive-fixnum,
minimum-integer, maximum-integer."
  )


(defconstant minimum-fixnum cl:most-negative-fixnum
  "The value of minimum-fixnum is cl:most-negative-fixnum.

These constants represent the minimum fixnum available in an
implementation.


Notes:

This is the parameter minint_I, with I equal to fixnum, required in
LIA1 [7]. Its value is the obvious Common Lisp counterpart.


See Also:

bounded-fixnum, bounded-integer, most-negative-fixnum,
most-positive-fixnum, minimum-integer, maximum-integer.
"
  )


(defconstant maximum-fixnum cl:most-positive-fixnum
  "The value of maximum-fixnum is cl:most-positive-fixnum.

These constant represents the maximum fixnum available in an
implementation.


Noes:

This is the parameter maxint_I, with I equal to fixnum, required in
LIA1 [7]. Its value is the obvious Common Lisp counterpart.


See Also:

bounded-fixnum, bounded-integer, most-negative-fixnum,
most-positive-fixnum, minimum-integer, maximum-integer.
")


(defconstant bounded-integer nil
  "The value is NIL.

This constant indicates that the Common Lisp datatype integer is
unbounded.

Notes:

This is the parameter bounded_I, with I equal to integer required in
LIA1 [7].


See Also:

bounded-fixnum, minimum-fixnum, most-negative-fixnum, maximum-fixnum,
most-positive-fixnum, minimum-integer, maximum-integer."
  )


(defconstant has-infinity-fixnum nil
  "The value is NIL.

This constant indicates that the Common Lisp datatype fixnum does not
include infinities.


Notes:

This is the parameter hasinf_I with I equal to fixnum, required in LIA1 [7].


See Also:

bounded-fixnum, bounded-integer, has-infinity-integer."
  )


(defconstant has-infinity-integer nil
  "The value is NIL.

This constant indicates that the Common Lisp datatype integer does not
include infinities.


Notes:

This is the parameter hasinf_I with I equal to integer, required in
LIA1.

Note that this parameter does not agree with the LIA1
specification; it is NIL and not T.  The reason is that at the time
of this writing there appear to be no portable way to express an
integer infinity in most (all?) Common Lisp implementations.


See Also:

bounded-fixnum, bounded-integer, has-infinity-integer."
  )


;;; The following constants do not make much sense and are left
;;; "undefined" for the time being.

(define-symbol-macro minimum-integer
  (cl-lia-impl-dep:raise-not-impl 'cl-lia-impl-dep:variable-not-implemented
                                  :name 'minimum-integer))

(define-symbol-macro maximum-integer
  (cl-lia-impl-dep:raise-not-impl 'cl-lia-impl-dep:variable-not-implemented
                                  :name 'maximum-integer))


;;;; end of file -- integral.lisp
