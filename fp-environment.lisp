;;;; -*- Mode: Lisp -*-

;;;; fp-environment.lisp
;;;;
;;;; Defintions relating to floating point "environments"
;;;; These definitions are not really present in the LIA
;;;; specifications, however, they are useful to implement the
;;;; "recording" of notifications.
;;;;
;;;; The functionalities presented here are patterned after the C
;;;; standards and the CMUCL/SBCL rendering of these concepts.
;;;;
;;;; See the file COPYING for license and copyright information.


(in-package "CL-LIA")


(deftype floating-point-environment ()
  "The actual floating-point-environment definition is implementation dependent.

The definition is used to specify the type of arguments being used by
the functions comprising the interface.
"
  'cl-lia-impl-dep:floating-point-environment)

  
(defun get-floating-point-environment ()
  "Return a representation of the current state of the floating point environment. 

The representation is a property list (a p-list). The format of the
returned modes a-list is such to be usable as an apply last argument
for set-floating-point-environment.


See Also:

set-floating-point-environment
"
  (cl-lia-impl-dep:get-floating-point-environment))


(defun set-floating-point-environment (&rest
                                       args
                                       &key
                                       (traps ())
                                       (rounding-mode :nearest)
                                       (current-notifications nil)
                                       (precision (float-precision 1.0))
                                       &allow-other-keys)
  "This function sets options controlling the floating-point hardware.

If a keyword is not supplied, then the current value is preserved.

The possible values for each of the keywords are the following.
- traps is a list that can contain the keywords :underflow, :overflow,
:inexact, :invalid, :divide-by-zero, and :denormalized-operand.
- rounding-mode is the rounding mode to use when the result is not exact;
it can assume the values :nearest, :positive-infinity,
:negative-infinity and :zero.
- current-notifications is used to set the exception flags. The main
use is setting the accrued exceptions to NIL to clear them.
- precision can be one of the integers 24, 53 and 64, standing for the
internal precision of the mantissa.


Exceptional Situations:

The function can always result in a no-op if access to the underlying
hardware is not fully supported. When this happens
set-floating-point-environment must issue a warning.


See Also:

get-floating-point-environment.
"
  (declare (ignore traps rounding-mode current-notifications precision))
  (apply #'cl-lia-impl-dep:set-floating-point-environment
         args))


(defun default-floating-point-environment ()
  "Return the default floating point environment.

The object returned represents the default floating point environment
in use by the implementation.


Notes:

The function default-floating-point-environment should always return
the same (or equalp) value.


See Also:

set-floating-point-environment, get-floating-point-environment.
"
  (cl-lia-impl-dep:default-floating-point-environment))


(defun restore-default-floating-point-environment ()
  "Restores the default Floating Point Environment."
  (cl-lia-impl-dep:restore-default-floating-point-environment))


(defmacro with-floating-point-environment ((&rest
                                            args
                                            &key
                                            (traps ())
                                            (rounding-mode :nearest)
                                            (current-notifications nil)
                                            (precision
                                             (float-precision 1.0))
                                            )
                                           &body
                                           body)
  "The macro executes BODY in a modified fp environment.

In the modified environment the floating point modes are determined by
the values passed as arguments to the macro. Upon termination (either
normal or exceptional) of the code in body the floating point modes
are restored to those in effect before the execution of
with-floating-point-environment.

As for set-floating-point-environment the values that the arguments
can take are the following:
- traps is a list that can contain the keywords :underflow, :overflow,
  :inexact, :invalid, :divide-by-zero, and :denormalized-operand.
- rounding-mode is the rounding mode to use when the result is not
  exact; it can assume the values :nearest, :positive-infinity,
  :negative-infinity and :zero.
- current-notifications is used to set the exception flags. The main
  use is setting the accrued exceptions to NIL to clear them.
- precision can be one of the integers 24, 53 and 64, standing for the
  internal precision of the mantissa.

The RESULTS is the value (or values) returned by body.


Notes:

When called with an empty arguments list,
with-floating-point-environment is a no-op and BODY is executed as-is.
"
  (declare (ignore traps rounding-mode current-notifications precision))
  `(cl-lia-impl-dep:with-floating-point-environment (,@args) ,@body))


;;; Accessors.
;;; Come back to fix the assumptions by deferring to the implementatin
;;; package.

(defun fpe-traps (&optional 
                  (fpe-plist (get-floating-point-environment))) 
  (getf fpe-plist :traps))

(defun fpe-rounding-mode (&optional
                          (fpe-plist (get-floating-point-environment)))
  (getf fpe-plist :rounding-mode :nearest))

(defun fpe-current-notifications (&optional
                                  (fpe-plist (get-floating-point-environment)))
  (getf fpe-plist :current-notifications))

(defun fpe-accrued-notifications (&optional
                                  (fpe-plist (get-floating-point-environment)))
  (getf fpe-plist :accrued-notifications))

(defun fpe-precision (&optional
                      (fpe-plist (get-floating-point-environment)))
  (getf fpe-plist :precision (cl:float-precision 1.0)))

(defun fpe-fast-mode-p (&optional
                        (fpe-plist (get-floating-point-environment)))
  (getf fpe-plist :fast-mode-p))


;;;; end of file -- fp-environment.lisp
