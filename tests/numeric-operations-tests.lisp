;;;; -*- Mode: Lisp -*-

;;;; numeric-operations-tests.lisp
;;;;
;;;; Tests regarding floating point exception handling..
;;;;
;;;; See the file COPYING in the main folder for license and copyright
;;;; information.


(in-package "CL-LIA-TESTS")

(use-package "FIVEAM")

(def-suite numeric-operations-tests
           :in cl-lia-test-suite
           :description
           "Tests checking floating point environment support.")

(in-suite numeric-operations-tests)

#+common-lisp
(test helper-wrap-functions
  (is (= 42
         (cl-lia::wrap-fixnum 42)))
  (is (= (1+ cl:most-positive-fixnum)
         (cl-lia::wrap-integer (1+ cl:most-positive-fixnum))))
  )


#+common-lisp
(test result-functions
  (is (= 42 (cl-lia::result-fixnum 42)))
  (is (= 42 (cl-lia::result-integer 42)))

  (let ((x 42.0))
    (is (= x (cl-lia::result-f x :nearest)))

    (is (= x (cl-lia::result-f x :nearest))))
  )





;;;; end of file -- numeric-operations-tests.lisp
