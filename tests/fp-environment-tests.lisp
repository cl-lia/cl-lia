;;;; -*- Mode: Lisp -*-

;;;; fp-environment-tests.lisp
;;;;
;;;; Tests regarding floating point exception handling..
;;;;
;;;; See the file COPYING in the main folder for license and copyright
;;;; information.


(in-package "CL-LIA-TESTS")

(use-package "FIVEAM")

(def-suite fp-environment-tests
           :in cl-lia-test-suite
           :description
           "Tests checking floating point environment support.")

(in-suite fp-environment-tests)

#+lispworks
(test fp-environment-queries
  (finishes (cl-lia:default-floating-point-environment))
  (finishes (cl-lia:get-floating-point-environment))
  (is (= (cl:float-precision 1.0)
         (cl-lia:fpe-precision
          (cl-lia:default-floating-point-environment))))
  )

#+lispworks
(test fp-environment-set

  (is (member :underflow
              (cl-lia:fpe-traps
               (cl-lia:set-floating-point-environment :traps '(:underflow)))))

  (is (member :division-by-zero
              (cl-lia:fpe-current-notifications
               (cl-lia:set-floating-point-environment :current-notifications
                                                      '(:division-by-zero)))))
  )


;;;; end of file -- fp-environment-tests.lisp
