;;;; -*- Mode: Lisp -*-

;;;; arithmetic-operations.lisp
;;;;
;;;; Arithmetic operations (cfr., 4.1)
;;;;
;;;; It is expected that an implementation will eventually provide
;;;; much lower level definitions.
;;;;
;;;; See the file COPYING for license and copyright information.

;;;; Notes
;;;; =====

;;;; 20210829 MA:
;;;; Note that no regard is given to efficiency: the code is
;;;; practically a transcription of the LIA 1 specification for the
;;;; sake of readability and cross checking.
;;;; It is clear that many tests are repeated and could be coalesced.

;;;; 20210828 MA:
;;;; All 'errors' do not yet follow the proper LIA notification
;;;; scheme. Come back to fix it.

;;;; 20211212 MA:
;;;; Changed all calls to ERROR to NOTIFY.

;;;; 20211219 MA:
;;;; Changed most INTEGER to RATIONAL.


;;;; Code
;;;; ====


(in-package "CL-LIA")

#|
  (:export
   "/=" "<" "<=" "=" ">=" ">"

   "EVENP" "ODDP"
   "PLUSP" "MINUSP"
   "ZEROP"
   "MIN" "MAX"
   )

  (:export
   "+" "*" "-" "/"

   "1+" "1-" "INCF" "DECF"

   "CONJUGATE"
   )

  (:export
   "+.<" "+.>"
   "*.<" "*.>"
   "-.<" "-.>"
   "/.<" "/.>"
   )

|#


;;; Implementation paramenters for arithmetic operations.


;;; modulo-I
;;; LIA 1 Appendix A.1

(defconstant modulo-fixnum nil)
(defconstant modulo-integer nil)


;;; Comparisons.
;;; Cfr LIA 1 5.1.2.1 and 5.2.6.1.

(defgeneric _=_ (n1 n2)
  (:documentation "Binary Equality operation.")

  (:method ((n1 rational) (n2 rational)) (result-i (cl:= n1 n2)))

  (:method ((n1 float) (n2 float))
   
   (cond ((or (is-signaling-nan n1) (is-signaling-nan n2))
          (notify 'floating-point-invalid-operation
                  :cv t
                  :operation '_=_
                  :operands (list n1 n2)))

         ((or (and (is-quiet-nan n1) (not (is-signaling-nan n2)))
              (and (is-quiet-nan n2) (not (is-signaling-nan n1))))
          nil)

         ((is-neg-zero n1) (_=_ 0 n2))
         ((is-neg-zero n2) (_=_ n1 0))

         ((or (and (is-positive-infinity n1)
                   (is-positive-infinity n2))
              (and (is-negative-infinity n1)
                   (is-negative-infinity n2)))
          t)

         ((or (is-infinity n1) (is-infinity n2)) nil) ; See case above...

         (t (cl:= n1 n2))
         ))

  (:method ((n1 rational) (n2 float))
   (_=_ (float n1 n2) n2))

  (:method ((n1 float) (n2 rational))
   (_=_ n1 (float n2 n1)))
  )


(defun = (n &rest numbers)
  (cond ((or (is-signaling-nan n) (some #'is-signaling-nan numbers))
         (notify 'floating-point-invalid-operation
                 :cv nil
                 :operation '=
                 :operands (cons n numbers)))
        ((is-quiet-nan n) nil)
        (t
         (loop for n1 in numbers
               always (_=_ n n1)))
        ))


(defgeneric _/=_ (n1 n2)
  (:documentation "Binary Inequality operation.")

  (:method ((n1 rational) (n2 rational)) (result-i (cl:/= n1 n2)))

  (:method ((n1 float) (n2 float))
   (not (_=_ n1 n2)))

  (:method ((n1 rational) (n2 float))
   (_/=_ (float n1 n2) n2))

  (:method ((n1 float) (n2 rational))
   (_/=_ n1 (float n2 n1)))
  )


(defun /= (n &rest numbers)
  (cond ((or (is-signaling-nan n) (some #'is-signaling-nan numbers))
         (notify 'floating-point-invalid-operation
                 :cv t
                 :operation '/=
                 :operands (cons n numbers)))
        ((is-quiet-nan n) t)
        (t
         (loop for n1 in numbers
               never (_=_ n n1)))
        ))


(defgeneric _<_ (n1 n2)
  (:documentation "Binary Less Than operation.")

  (:method ((n1 rational) (n2 rational)) (result-i (cl:< n1 n2)))

  (:method ((n1 float) (n2 float))
   
   (cond ((or (is-nan n1) (is-nan n2))
          (notify 'floating-point-invalid-operation
                  :cv nil
                  :operation '_<_
                  :operands (list n1 n2)))

         ((is-positive-infinity n2) t)

         ((is-negative-infinity n2) nil)

         ((is-positive-infinity n1) nil)

         ((is-negative-infinity n1) t)

         ((is-neg-zero n2) (_<_ n1 0))

         ((is-neg-zero n1) (_<_ 0 n2))

         (t (cl:< n1 n2))
         ))

  (:method ((n1 rational) (n2 float))
   (_<_ (float n1 n2) n2))

  (:method ((n1 float) (n2 rational))
   (_<_ n1 (float n2 n1)))
  )


(defun < (n &rest numbers)
  (cond ((or (is-nan n) (some #'is-nan numbers))
         (notify 'floating-point-invalid-operation
                 :cv t
                 :operation '<
                 :operands (cons n numbers)))
        ((null numbers) t)

        (t (and (_<_ n (first numbers))
                (loop for (n1 n2) on numbers
                      always (if n2 (_<_ n1 n2) t))))
        ))


(defgeneric _<=_ (n1 n2)
  (:documentation "Binary Less Than Or Equal operation.")

  (:method ((n1 rational) (n2 rational)) (result-i (cl:<= n1 n2)))

  (:method ((n1 float) (n2 float))
   
   (cond ((or (is-nan n1) (is-nan n2))
          (notify 'floating-point-invalid-operation
                  :cv nil
                  :operation '_<=_
                  :operands (list n1 n2)))

         ((is-negative-infinity n1) t)

         ((is-positive-infinity n1) (is-positive-infinity n2))

         ((is-negative-infinity n1) t)

         ((is-neg-zero n2) (_<=_ n1 0))

         ((is-neg-zero n1) (_<=_ 0 n2))

         (t (cl:<= n1 n2))
         ))

  (:method ((n1 rational) (n2 float))
   (_<=_ (float n1 n2) n2))

  (:method ((n1 float) (n2 rational))
   (_<=_ n1 (float n2 n1)))
  )


(defun <= (n &rest numbers)
  (cond ((or (is-nan n) (some #'is-nan numbers))
         (notify 'floating-point-invalid-operation
                 :cv t
                 :operation '<=
                 :operands (cons n numbers)))
        ((null numbers) t)

        (t (and (_<=_ n (first numbers))
                (loop for (n1 n2) on numbers
                      always (if n2 (_<=_ n1 n2) t))))
        ))


(defgeneric _>_ (n1 n2)
  (:documentation "Binary Greater Than operation.")

  (:method ((n1 real) (n2 real)) (_<_ n2 n1)))


(defun > (n &rest numbers)
  (cond ((or (is-nan n) (some #'is-nan numbers))
         (notify 'floating-point-invalid-operation
                 :cv t
                 :operation '>
                 :operands (cons n numbers)))
        ((null numbers) t)

        (t (and (_>_ n (first numbers))
                (loop for (n1 n2) on numbers
                      always (if n2 (_>_ n1 n2) t))))
        ))


(defgeneric _>=_ (n1 n2)
  (:documentation "Binary Greater Than operation.")

  (:method ((n1 real) (n2 real)) (_<=_ n2 n1)))


(defun >= (n &rest numbers)
  (cond ((or (is-nan n) (some #'is-nan numbers))
         (notify 'floating-point-invalid-operation
                 :cv t
                 :operation '>=
                 :operands (cons n numbers)))
        ((null numbers) t)

        (t (and (_>=_ n (first numbers))
                (loop for (n1 n2) on numbers
                      always (if n2 (_>=_ n1 n2) t))))
        ))


;;; Sum.
;;; Cfr., LIA 1, 5.2.6.2

(defgeneric _+_ (n1 n2)
  (:documentation "Binary plus operation.")

  (:method ((n1 rational) (n2 rational)) (result-i (cl:+ n1 n2)))
  
  (:method ((n1 float) (n2 float))
   
   (cond ((or (is-nan n1) (is-nan n2))
          (no-result-f-2 n1 n2 '+))

         ((or (and (is-negative-infinity n1)
                   (is-positive-infinity n2))
              (and (is-positive-infinity n1)
                   (is-negative-infinity n2)))

          (no-result-f-2 n1 n2 '+)
          )

         ((and (is-neg-zero n1) (is-neg-zero n2)) n1)

         ((is-infinity n1) n1)

         ((is-infinity n2) n2)

         (t (result-f (cl:+ n1 n2) (get-rounding-mode)))
         ))

  (:method ((n1 rational) (n2 float))
   (_+_ (cl:float n1 n2) n2))

  (:method ((n1 float) (n2 rational))
   (_+_ n1 (cl:float n2 n1)))

  )


(defun + (&rest numbers)
  (reduce #'_+_ numbers :initial-value 0))


;;; Difference.

(defgeneric _-_ (n1 n2)
  (:documentation "Binary subtract operation.")

  (:method ((n1 rational) (n2 rational)) (result-i (cl:- n1 n2)))
  
  (:method ((n1 float) (n2 float))
   (cond ((or (is-nan n1) (is-nan n2))
          (no-result-f-2 n1 n2 '-))

         ((or (and (is-negative-infinity n1)
                   (is-negative-infinity n2))
              (and (is-positive-infinity n1)
                   (is-positive-infinity n2)))

          (no-result-f-2 n1 n2 '-)
          )

         ((and (is-neg-zero n1) (is-neg-zero n2)) n1)

         ((is-infinity n1) n1)

         ((is-infinity n2) n2)

         (t (result-f (cl:- n1 n2) (get-rounding-mode)))
         ))

  (:method ((n1 rational) (n2 float))
   (_-_ (cl:float n1 n2) n2))

  (:method ((n1 float) (n2 rational))
   (_-_ n1 (cl:float n2 n1)))
  )


(defgeneric -_ (n)
  (:documentation "Unary subtract operation.")

  (:method ((n rational)) (result-i (cl:- n)))
  
  (:method ((n float))
   (cond ((is-nan n) (no-result-f n '-))

         ((is-neg-zero n) (cl:- n))

         ((is-positive-infinity n)
          (cl:float short-float-negative-infinity n))

         ((is-negative-infinity n)
          (cl:float short-float-positive-infinity n))

         (t (result-f (cl:- n) (get-rounding-mode)))
         ))
  )


(defun - (n &rest numbers)
  (cond ((null numbers) (-_ n))
        ((null (cdr numbers)) (_-_ n (first numbers)))
        (t
         (apply #'reduce #'_-_ n numbers))
        ))


;;; Multiplication.

(defgeneric _*_ (n1 n2)
  (:documentation "Binary multiplication operation.")

  (:method ((n1 rational) (n2 rational)) (result-i (cl:* n1 n2)))
  
  (:method ((n1 float) (n2 float))
   (cond ((or (is-nan n1) (is-nan n2))
          (no-result-f-2 n1 n2 '*))

         ((is-infinity n1)
          (if (cl:plusp (signum n2))
              n1
              (-_ n1)))

         ((is-infinity n2)
          (if (cl:plusp (signum n1))
              n2
              (-_ n2)))

         (t (result-f (cl:* n1 n2) (get-rounding-mode)))
         ))

  (:method ((n1 rational) (n2 float))
   (_*_ (cl:float n1 n2) n2))

  (:method ((n1 float) (n2 rational))
   (_*_ n1 (cl:float n2 n1)))
  )


(defun * (&rest numbers)
  (reduce #'_*_ numbers :initial-value 1))


;;; Division.

(defgeneric _/_ (n1 n2)
  (:documentation "Binary divide operation.

The implementation relies follows the specification of LIA-1 for
rational and float values.

Notes:

The implementation defers, as it must, to the underlying
implementation of rounding operations, which may or may not be
defined.
")

  (:method ((n1 rational) (n2 rational))
   (result-i (cl:/ n1 n2)) ; Catch the DIVISION-BY-ZERO here.
   )

  (:method ((n1 rational) (n2 float))
   (_/_ (float n1 n2) n2))


  (:method ((n1 float) (n2 rational))
   (_/_ n1 (float n2 n1)))
  
  (:method ((n1 float) (n2 float))

   ;; The conditions below reflect the LIA 1 specification, although
   ;; there is no negative zero in CL.

   (cond ((or (is-nan n1) (is-nan n2))
          (no-result-f-2 n1 n2 '/))

         ((and (finitep n1) (finitep n2) (cl:/= 0.0 n1) (cl:/= 0.0 n2))
          (result-f (cl:/ n1 n2) (get-rounding-mode))
          )

         ;; N1 is 0 or -0.

         ((and (zerop n1) (finitep n2) (plusp n2))
          (coerce n1 (larger-float-type (type-of n1) (type-of n2))))

         ((and (zerop n1) (finitep n2) (minusp n2)) ; Pleonastic in CL.
          (- (coerce n1 (larger-float-type (type-of n1) (type-of n2)))))

         ((and (is-neg-zero n1) (finitep n2) (plusp n2)) ; Pleonastic in CL.
          (coerce n1 (larger-float-type (type-of n1) (type-of n2))))

         ((and (is-neg-zero n1) (finitep n2) (minusp n2)) ; Pleonastic in CL.
          (coerce 0.0 (larger-float-type (type-of n1) (type-of n2))))


         ;; N2 is 0 or -0.

         ((and (zerop n2) (finitep n1) (plusp n1))
          (notify 'infinitary
                  :operation '_/_
                  :operands (list n1 n2)
                  :continuation-value
                  (select-infinity-of-type
                   (larger-float-type (type-of n1) (type-of n2))
                   t)))

         ((and (zerop n2) (finitep n1) (minusp n2))
          (notify 'infinitary
                  :operation '_/_
                  :operands (list n1 n2)
                  :continuation-value
                  (select-infinity-of-type
                   (larger-float-type (type-of n1) (type-of n2))
                   nil)))

         ((and (is-neg-zero n2) (finitep n1) (plusp n1)) ; Pleonastic in CL.
          (notify 'infinitary
                  :operation '_/_
                  :operands (list n1 n2)
                  :continuation-value
                  (select-infinity-of-type
                   (larger-float-type (type-of n1) (type-of n2))
                   nil)))

         ((and (is-neg-zero n2) (finitep n1) (minusp n2)) ; Pleonastic in CL.
          (notify 'infinitary
                  :operation '_/_
                  :operands (list n1 n2)
                  :continuation-value
                  (select-infinity-of-type
                   (larger-float-type (type-of n1) (type-of n2))
                   t)))

         
         ;; N2 is an infinity.

         ((and (finitep n1) (cl:>= n1 0.0) (is-positive-infinity n2))
          (coerce 0.0 (larger-float-type (type-of n1) (type-of n2))))

         ((and (finitep n1) (cl:>= n1 0.0) (is-negative-infinity n2))
          (- (coerce 0.0 (larger-float-type (type-of n1) (type-of n2)))))

         ((and (or (and (finitep n1) (minusp n1)) (is-neg-zero n1))
               (is-positive-infinity n2)) ; Pleonastic in CL.
          (- (coerce 0.0 (larger-float-type (type-of n1) (type-of n2)))))

         ((and (or (and (finitep n1) (minusp n1)) (is-neg-zero n1))
               (is-negative-infinity n2)) ; Pleonastic in CL.
          (coerce 0.0 (larger-float-type (type-of n1) (type-of n2))))

         ))
  )


(defun / (n &rest numbers)
  (cond ((null numbers)
         (_/_ n 1))
        ((null (cdr numbers))
         (_/_ n (first numbers)))
        (t
         (apply #'reduce #'_/_ n numbers))
        ))


;;;; Rounding versions of basic functions.
;;;; -------------------------------------

(defgeneric _+.<_ (n1 n2)
  (:documentation "Binary plus operation rounding down.")

  (:method ((n1 rational) (n2 rational)) (_+_ n1 n2))

  (:method ((n1 float) (n2 float))
   (if (and (is-finite n1) (is-finite n2))
       (result-f (cl:+ n1 n2) :negative-infinity)
       (_+_ n1 n2)))

  (:method ((n1 rational) (n2 float))
   (_+.<_ (float n1 n2) n2))

  (:method ((n1 float) (n2 rational))
   (_+.<_ n1 (float n2 n1)))
  )


(defun +.< (&rest numbers)
  (reduce #'_+.<_ numbers :initial-value 0))


(defgeneric _+.>_ (n1 n2)
  (:documentation "Binary plus operation rounding up.")

  (:method ((n1 rational) (n2 rational)) (_+_ n1 n2))

  (:method ((n1 float) (n2 float))
   (if (and (is-finite n1) (is-finite n2))
       (result-f (_+.>_ n1 n2) :positive-infinity)
       (_+_ n1 n2)))

  (:method ((n1 rational) (n2 float))
   (_+.>_ (float n1 n2) n2))

  (:method ((n1 float) (n2 rational))
   (_+.>_ n1 (float n2 n1)))
  )


(defun +.> (&rest numbers)
  (reduce #'_+.>_ numbers :initial-value 0))


(defgeneric _+.<>_ (n1 n2)
  (:documentation "Binary plus operation rounding nearest.")

  (:method ((n1 rational) (n2 rational)) (_+_ n1 n2))

  (:method ((n1 float) (n2 float))
   (if (and (is-finite n1) (is-finite n2))
       (result-f (_+.<>_ n1 n2) :nearest)
       (_+_ n1 n2)))

  (:method ((n1 rational) (n2 float))
   (_+.<>_ (float n1 n2) n2))

  (:method ((n1 float) (n2 rational))
   (_+.<>_ n1 (float n2 n1)))
  )


(defun +.<> (&rest numbers)
  (reduce #'_+.<>_ numbers :initial-value 0))


(defgeneric _-.<_ (n1 n2)
  (:documentation "Binary plus operation rounding down.")

  (:method ((n1 rational) (n2 rational)) (_-_ n1 n2))

  (:method ((n1 float) (n2 float))
   (if (and (is-finite n1) (is-finite n2))
       (result-f (_-.<_ n1 n2) :negative-infinity)
       (_-_ n1 n2)))

  (:method ((n1 rational) (n2 float))
   (_-.<_ (float n1 n2) n2))

  (:method ((n1 float) (n2 rational))
   (_-.<_ n1 (float n2 n1)))
  )


(defgeneric _-.>_ (n1 n2)
  (:documentation "Binary plus operation rounding up.")

  (:method ((n1 rational) (n2 rational)) (_-_ n1 n2))

  (:method ((n1 float) (n2 float))
   (if (and (is-finite n1) (is-finite n2))
       (result-f (_-.>_ n1 n2) :positive-infinity)
       (_-_ n1 n2)))

  (:method ((n1 rational) (n2 float))
   (_-.>_ (float n1 n2) n2))

  (:method ((n1 float) (n2 rational))
   (_-.>_ n1 (float n2 n1)))
  )


(defgeneric _-.<>_ (n1 n2)
  (:documentation "Binary plus operation rounding nearest.")

  (:method ((n1 rational) (n2 rational)) (_-_ n1 n2))

  (:method ((n1 float) (n2 float))
   (if (and (is-finite n1) (is-finite n2))
       (result-f (_-.<>_ n1 n2) :nearest)
       (_-_ n1 n2)))

  (:method ((n1 rational) (n2 float))
   (_-.<>_ (float n1 n2) n2))

  (:method ((n1 float) (n2 rational))
   (_-.<>_ n1 (float n2 n1)))
  )


(defgeneric _*.<_ (n1 n2)
  (:documentation "Binary multiplication operation rounding down.")

  (:method ((n1 rational) (n2 rational)) (_*_ n1 n2))

  (:method ((n1 float) (n2 float))
   (if (and (is-finite n1) (is-finite n2))
       (result-f (_*.<_ n1 n2) :negative-infinity)
       (_*_ n1 n2)))

  (:method ((n1 rational) (n2 float))
   (_*.<_ (float n1 n2) n2))

  (:method ((n1 float) (n2 rational))
   (_*.<_ n1 (float n2 n1)))
  )


(defgeneric _*.>_ (n1 n2)
  (:documentation "Binary multiplication operation rounding up.")

  (:method ((n1 rational) (n2 rational)) (_*_ n1 n2))

  (:method ((n1 float) (n2 float))
   (if (and (is-finite n1) (is-finite n2))
       (result-f (_*.>_ n1 n2) :positive-infinity)
       (_*_ n1 n2)))

  (:method ((n1 rational) (n2 float))
   (_*.>_ (float n1 n2) n2))

  (:method ((n1 float) (n2 rational))
   (_*.>_ n1 (float n2 n1)))
  )


(defgeneric _*.<>_ (n1 n2)
  (:documentation "Binary multiplication operation rounding nearest.")

  (:method ((n1 rational) (n2 rational)) (_*_ n1 n2))

  (:method ((n1 float) (n2 float))
   (if (and (is-finite n1) (is-finite n2))
       (result-f (_*.<>_ n1 n2) :nearest)
       (_+_ n1 n2)))

  (:method ((n1 rational) (n2 float))
   (_*.<>_ (float n1 n2) n2))

  (:method ((n1 float) (n2 rational))
   (_*.<>_ n1 (float n2 n1)))
  )


(defgeneric _/.<_ (n1 n2)
  (:documentation "Binary multiplication operation rounding down.")

  (:method ((n1 rational) (n2 rational)) (_/_ n1 n2))

  (:method ((n1 float) (n2 float))
   (if (and (is-finite n1) (is-finite n2))
       (result-f (_/.<_ n1 n2) :negative-infinity)
       (_/_ n1 n2)))

  (:method ((n1 rational) (n2 float))
   (_/.<_ (float n1 n2) n2))

  (:method ((n1 float) (n2 rational))
   (_/.<_ n1 (float n2 n1)))
  )


(defgeneric _/.>_ (n1 n2)
  (:documentation "Binary multiplication operation rounding up.")

  (:method ((n1 rational) (n2 rational)) (_/_ n1 n2))

  (:method ((n1 float) (n2 float))
   (if (and (is-finite n1) (is-finite n2))
       (result-f (_/.>_ n1 n2) :positive-infinity)
       (_/_ n1 n2)))

  (:method ((n1 rational) (n2 float))
   (_/.>_ (float n1 n2) n2))

  (:method ((n1 float) (n2 rational))
   (_/.>_ n1 (float n2 n1)))
  )


(defgeneric _/.<>_ (n1 n2)
  (:documentation "Binary multiplication operation rounding nearest.")

  (:method ((n1 rational) (n2 rational)) (_/_ n1 n2))

  (:method ((n1 float) (n2 float))
   (if (and (is-finite n1) (is-finite n2))
       (result-f (_/.<>_ n1 n2) :nearest)
       (_/_ n1 n2)))

  (:method ((n1 rational) (n2 float))
   (_/.<>_ (float n1 n2) n2))

  (:method ((n1 float) (n2 rational))
   (_/.<>_ n1 (float n2 n1)))
  )


;;;; Other functions.
;;;; ----------------

(defgeneric abs (x)
  (:method ((x real))
   (cond ((finitep x) (cl:abs x))
         ((is-neg-zero x) (float 0.0s0 x))
         ((is-positive-infinity x) x)
         ((is-negative-infinity x) (- x))
         (t (no-result-f x)))))


(defgeneric signum (x)
  (:method ((x rational))
   (cl:signum x))

  (:method ((x float))
   (cond ((or (and (finitep x) (cl:>= x 0.0)) (is-positive-infinity x))
          (float 1.0s0 x))

         ((or (and (finitep x) (cl:minusp x))
              (is-negative-infinity x)
              (is-neg-zero x))
          (float -1.0s0 x))

         (t (no-result-f x))))
  )


(defgeneric residue (x y)
  (:method ((x float) (y float))
   (cond ((and (finitep x) (finitep y))
          (handler-case
              (multiple-value-bind (rnd rm)
                  (cl:fround x y)
                (declare (ignore rnd))
                rm)
            (cl:division-by-zero (dbz)
              (declare (ignore dbz))
              ;; y is 'zero'.
              (if (is-neg-zero y) ; Pleonastic in CL.
                  (notify 'infinitary
                          :operation 'residue
                          :operands (list x y)
                          :continuation-value
                          (select-infinity-of-type
                           (larger-float-type (type-of x) (type-of y))
                           (cl:minusp x)))

                  ;; 'zero' as usual.
                  (notify 'infinitary
                          :operation 'residue
                          :operands (list x y)
                          :continuation-value
                          (select-infinity-of-type
                           (larger-float-type (type-of x) (type-of y))
                           (not (cl:minusp x))))
                  )))
          )

         ((and (is-neg-zero x) (not (is-nan y)) (cl:/= 0.0 y))
          x) ; Pleonastic in CL.

         ((and (finitep x) (is-infinity y))
          x) ; This seem strange, but it follows the LIA 1 spec.

         (t
          (no-result-f x y))
         ))

  (:method ((x rational) (y rational))
   (handler-case
       (multiple-value-bind (rnd rm)
           (cl:round x y)
         (declare (ignore rnd))
         rm)
     (cl:division-by-zero (dbz)
       (declare (ignore dbz))
       ;; y is 'zero'.
       (if (is-neg-zero y) ; Pleonastic in CL.
           (notify 'infinitary
                   :operation 'residue
                   :operands (list x y)
                   :continuation-value
                   (select-infinity-of-type
                    *read-default-float-format*
                    (cl:minusp x)))
           
           ;; 'zero' as usual.
           (notify 'infinitary
                   :operation 'residue
                   :operands (list x y)
                   :continuation-value
                   (select-infinity-of-type
                    *read-default-float-format*
                    (not (cl:minusp x))))
           ))))

  (:method ((x rational) (y float))
   (residue (float x y) y))

  (:method ((x float) (y rational))
   (residue x (float y x)))

  )


(defgeneric sqrt (x)
  (:method ((x real))
   (cond ((and (finitep x) (cl:>= x 0.0)) (cl:sqrt x))
         ((or (is-neg-zero x) (is-positive-infinity x)) x)
         (t (no-result-f x))))
  )
                   

(defgeneric isqrt (x)
  (:method ((x integer))
   (if (cl:>= x 0)
       (cl:isqrt x)
       (no-result-f x) ; This is wrong.  Come back later to fix.
       ))

  (:method ((x t))
   (notify 'type-error ; This is semi-wrong.  Come back and fix later.
          :datum x
          :expected-type 'integer))
  )


;;;; Value dissection (cfr., LIA 1, 5.2.6.3)
;;;; ---------------------------------------
;;;;
;;;; Note:
;;;; Come back and fix error catching and signaling and various
;;;; checks.

(defgeneric exponent (x)
  (:method ((x float))
   (cond ((and (is-finite x) (not (zerop x)))
          (nth-value 1 (cl:decode-float x)))

         ((or (is-zero x) (is-neg-zero x))
          (notify 'infinitary
                  :cv (select-infinity-of-type x nil)
                  :operator 'exponent
                  :operands (list x)))

         ((is-infinity x) (float infs x))

         ((is-quiet-nan x) q-nan)

         ((is-signaling-nan x)
          (notify 'floating-point-invalid-operation
                  :cv q-nan
                  :operator 'exponent
                  :operands (list x)))
         ))
  )


(defgeneric fraction (x)
  (:method ((x float))
   (cond ((is-finite x) (nth-value 0 (cl:decode-float x)))
         ((is-infinity x) x)
         (t (no-result-f x 'fraction))
         ))
  )


(defgeneric scale-float (f n)
  (:method ((f float) (n integer))
   (result-f (cl:scale-float f n) :nearest))
   )


(defgeneric succ (x)
  (:method ((x float)) (cl-lia-impl-dep::succ x)))


(defgeneric pred (x)
  (:method ((x float)) (cl-lia-impl-dep::pred x)))


(defgeneric ulp (x)
  (:method ((x float)) (cl-lia-impl-dep::ulp x)))


;;; Value splitting (cfr., LIA 1, 5.2.6.4)
;;;; ---------------------------------------
;;;;
;;;; Note:
;;;; Come back and fix error catching and signaling and various
;;;; checks.

(defgeneric int-part (x)
  (:method ((x float))
   (cond ((or (is-neg-zero x) (is-infinity x)) x)
         ((finitep x)
          (nth-value 0 (cl:ftruncate x)))
         (t
          (no-result-f x 'int-part))
         )))
   


(defgeneric fract-part (x)
  (:method ((x float))
   (cond ((is-neg-zero x) x)
         ((finitep x)
          (nth-value 1 (cl:ftruncate x)))
         (t
          (no-result-f x 'fract-part))
         )))


(defgeneric truncate-float (x n)
  (:method ((x float) (n integer))
   (cond ((or (is-neg-zero x) (is-infinity x)) x)
         ((finitep x) (cl:ftruncate x n))
         (t
          (no-result-f-2 x n 'truncate-float))
         )))


(defgeneric round-float (x n)
  (:method ((x float) (n integer))
   (cond ((or (is-neg-zero x) (is-infinity x)) x)
         ((finitep x) (cl:fround x n))
         (t
          (no-result-f-2 x n 'round-float))
         )))


;;;; end of file -- arithmetic-operations.lisp
