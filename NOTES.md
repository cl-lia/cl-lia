# CL-LIA Design and Implementation Notes

The implementation follows the suggestions contained in the LIA
specification documents.

The actual code is, for the time being, organized in "three" levels:
files containing shared definitions, with a "common" suffix, the
`impl-dependent` folder, which contains code pertaining the different
Common Lisp implementations, and the "implementation" files which
contain the main code, which depends on the previous two.  The
structure of the `.asd` and `.system` files reflects this
organitation.

