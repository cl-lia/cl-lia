;;;; -*- Mode: Lisp -*-

;;;; notification-style-common.lisp
;;;;
;;;; Lispworks defintions relating to the different notification styles.
;;;;
;;;; These abbreviations are used in the text.
;;;;
;;;; * NRI : notification by recording in indicators (i.e., think C).
;;;; * NACF: notification by alteration of control flow (i.e., think
;;;;         "exceptions")
;;;; * NTM : notification by termination (i.e., think "exit" with code).
;;;;
;;;; See the file COPYING for license and copyright information.


(in-package "CL-LIA")


(deftype notification-style ()
  '(member :recording   ; NRI
           :error       ; NACF
           :terminating ; NTM
           ))


;;;; end of file -- notification-style-common.lisp
