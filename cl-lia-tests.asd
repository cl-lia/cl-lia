;;;; -*- Mode: Lisp -*-

;;;; cl-lia-tests.asd
;;;; 
;;;; The CL-LIA-TESTS ASDF system.
;;;;
;;;; See the file COPYING for license and copyright information.

(eval-when (:load-toplevel :compile-toplevel :execute)
  (unless (find-package "FIVEAM")
    (error "The FIVEAM test framework is required.")))


(asdf:defsystem "CL-LIA-TESTS"
  :description "The CL LIA library."
  
  :author "Marco Antoniotti"

  :license "BSD"

  :depends-on ("CL-LIA")

  :initially-do 

  :components ((:module "tests"
                :source-pathname "tests/"
                :components ((:file "cl-lia-tests-pkg")
                             (:file "cl-lia-test-suite"
                              :depends-on ("cl-lia-tests-pkg"))
                             (:file "non-comp-queries-tests"
                              :depends-on ("cl-lia-test-suite"))
                             ))
               )
  )

;;;; end of file -- cl-lia.asd
