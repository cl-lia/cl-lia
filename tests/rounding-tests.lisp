;;;; -*- Mode: Lisp -*-

;;;; rounding-tests.lisp
;;;;
;;;; Tests regarding floating point exception handling..
;;;;
;;;; See the file COPYING in the main folder for license and copyright
;;;; information.


(in-package "CL-LIA-TESTS")

(use-package "FIVEAM")

(def-suite rounding-tests
           :in cl-lia-test-suite
           :description
           "Tests checking floating point environment support.")

(in-suite rounding-tests)


(test rounding-queries
  (is (typep cl-lia:rounding-style-float 'cl-lia:rounding-style))
  (is (typep (cl-lia:get-rounding-mode) 'cl-lia:rounding-mode))
  (is (eq :nearest (cl-lia:default-rounding-mode))) ; Default.
  )


(test (rounding-set :depends-on rounding-queries)
  (if (eq :nearest-ties-to-even cl-lia:rounding-style-float)
      (is (eq :positive-infinity
              (cl-lia:set-rounding-mode :positive-infinity)))
      (signals error
        (cl-lia:set-rounding-mode :positive-infinity))
      ))


(test (rounding-macro-tests :depends-on rounding-queries)
  (let ((x 42.0))
    (is (= x (cl-lia:round-to-near x))))

  (cond ((eq :nearest-ties-to-even cl-lia:rounding-style-float)
         (finishes (cl-lia:round-to-zero 42.0))
         (finishes (cl-lia:round-upward 42.0))
         (finishes (cl-lia:round-downward 42.0))
         )
        (t
         (signals error (cl-lia:round-to-zero 42.0))
         (signals error (cl-lia:round-upward 42.0))
         (signals error (cl-lia:round-downward 42.0)))
        ))


#-lispworks
(test (with-rounding-tests :depends-on rounding-set)
  (is (cl:eq :nearest
             (cl-lia:with-rounding-mode (:nearest)
               (cl-lia:get-rounding-mode))))

  (is (cl:eq :zero
             (cl-lia:with-rounding-mode (:zero)
               (cl-lia:get-rounding-mode))))

  (cl-lia:with-rounding-mode (:zero)
    (cl-lia:with-rounding-mode (:positive-infinity)
      (is (cl:eq :positive-infinity
                 (cl-lia:get-rounding-mode))))
    (is (cl:eq :zero
               (cl-lia:get-rounding-mode))))
  )

#+lispworks
(test with-rounding-tests
  (signals error
    (cl-lia:with-rounding-mode (:nearest)
      (cl-lia:get-rounding-mode)))

  (signals error
    (cl-lia:with-rounding-mode (:zero)
      (cl-lia:get-rounding-mode)))

  (signals error
    (cl-lia:with-rounding-mode (:zero)
      (cl-lia:with-rounding-mode (:positive-infinity)
        (is (cl:eq :positive-infinity
                   (cl-lia:get-rounding-mode))))
      (is (cl:eq :zero
                 (cl-lia:get-rounding-mode)))))
  )

;;;; end of file -- rounding-tests.lisp
