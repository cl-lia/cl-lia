;;;; -*- Mode: Lisp -*-

;;;; cl-lia-common.lisp
;;;;
;;;; Common defintions for all CL-LIA
;;;;
;;;; See the file COPYING for license and copyright information.


(in-package "CL-LIA")


;;; lia-error

(define-condition lia-error (error) ()
  (:documentation
   "The Lia Error Condition.

A 'mixin' class that serves as a root of all the LIA related conditions.
"
   )
  )


(define-condition lia-simple-error (simple-error) ()
  (:documentation
   "The Lia Simple Error Condition."
   )
  (:report
   (lambda (lse stream)
     (format stream
             "CL-LIA: ~?"
             (simple-condition-format-control lse)
             (simple-condition-format-arguments lse))))
  )


;;;; end of file -- cl-lia-common.lisp
