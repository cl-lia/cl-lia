;;;; -*- Mode: Lisp -*-

;;;; fp-exception-handling.lisp
;;;;
;;;; Defintions relating to floating points exception handling (cfr., 3.3).
;;;;
;;;; See the file COPYING for license and copyright information.


(in-package "CL-LIA")


(deftype fpe-notification-set ()
  "Sets of fpe-notification objects.

Objects of this type have an implementation dependent representation.

Notes:

The actual implementation may be a list, an (unsigned-byte 8), or some
other opaque object.  Users should not rely on a particular underlying
implementation."
  'cl-lia-impl-dep:fpe-notification-set)


(defconstant +fpe-all-notifications+
  cl-lia-impl-dep:+fpe-all-notifications+
  "Holds a representation the set of supported exceptions.

The elements are of type fpe-notification. They represent what the
implementation supports.

See Also:

fpe-notification, fpe-notification-set, all-supported-notifications-p,
some-supported-notifications-p, supported-notification-p.
"
  )


(defun supported-notification-p (fpe-notfn)
  "The function returns T if fpe-notfn is supported by the implementation.

Exceptional Situation:

The functions signal a type-error if fpe-notfn is not of type
fpe-notification.
"
  (cl-lia-impl-dep:supported-notification-p fpe-notfn))
  

(defun all-supported-notifications-p (&rest fpe-notfns)
  "The function returns T if all of fpe-notfns are supported by the implementation.

The functions signal a type-error if fpe-notfns contains objects not
of type fpe-notification.
"
  (apply #'cl-lia-impl-dep:all-supported-notifications-p fpe-notfns))


(defun some-supported-notifications-p (&rest fpe-notfns)
  "The function returns T if some of fpe-notfn are supported by the implementation.

The functions signal a type-error if fpe-notfns contains objects not
of type fpe-notification.
"
  (apply #'cl-lia-impl-dep:some-supported-notifications-p fpe-notfns))


(defun fpe-test-notifications (&rest excps)
  "The function tests which of the excps is set.

I.e., the function tests whether the corresponding flag is set in the
underlying floating point environment and returns an object of type
fpe-notification-set with the corresponding flag set.


Examples:

The following example (adapted from [4]) shows how a piece of code may
decide how to handle either floating-point-invalid-operation or
floating-point-overflow (cfr., [2].)

    (let ((fpe-excps (fpe-test-notifications :overflow :invalid)))
      (when (fpe-check-notifications :invalid)
        (error 'cl:floating-point-invalid-operation))
      (when (fpe-check-notifications :overflow)
        (error 'cl:floating-point-overflow))
      )


Notes:

The fpe-test-notifications function accesses the current floating
point environment.  Common Lisp implementations may have made
different choices about if, when, and how to signal the standard
Common Lisp floating point conditions. That is, the above example may
or may not work in a given Common Lisp implementation, as the code
that actually set the floating environment exception flags may have
already signaled either cl:floating-point-invalid-operation or
cl:floating-point-overflow, and the some corresponding handling code
may have already cleared the flags.


See Also:

fpe-notification, fpe-notification-set, fpe-check-notifications,
cl:floating-point-invalid-operation, cl:floating-point-overflow.
"
  (apply #'cl-lia-impl-dep:fpe-test-notifications excps))


(defun test-indicators (&rest excpts)
  "A synonim for FPE-TEST-NOTIFICATIONS.

See Also:

FPE-TEST-NOTIFICATIONS.
"
  (apply #'fpe-test-notifications excpts))


(defun fpe-check-notifications (&rest excps)
  "The function checks whether all of the excps flags are set in the excp-set.

The function returns a boolean indicating the result. If the excps is
empty, then the function returns NIL.


Examples:

The following example (adapted from [4]) shows how a piece of code may
decide how to signal either floating-point-invalid-operation or
floating-point-overflow (cfr., [2].)

    (let ((fpe-excps (fpe-test-notifications :overflow :invalid)))
      (when (fpe-check-notifications :invalid)
        (signal 'cl:floating-point-invalid-operation))
      (when (fpe-check-notifications :overflow)
        (signal 'cl:floating-point-overflow))
      )


Notes:

Note that the value returned when excps is empty is nor what Common
Lisp users may expect from a function that looks like a call to (and).
The notes about fpe-test-notifications regarding the example above
apply also in the case of fpe-check-notifications.


See Also:

fpe-notification, fpe-notification-set, fpe-test-notifications,
cl:floating-point-invalid-operation, cl:floating-point-overflow.
"
  (apply #'cl-lia-impl-dep:fpe-check-notifications excps))


(defun fpe-clear-notifications (excp-set &rest excps)
  (declare (type fpe-notification-set excp-set))
  (apply #'cl-lia-impl-dep:fpe-clear-notifications excp-set excps))


(defun clear-indicators (excp-set &rest excpts)
  "A synonim for FPE-CLEAR-NOTIFICATIONS.

See Also:

FPE-CLEAR-NOTIFICATIONS.
"
  (apply #'fpe-clear-notifications excp-set excpts))


;;;; end of file -- fp-exception-handling.lisp
