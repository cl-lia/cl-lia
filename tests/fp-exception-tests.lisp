;;;; -*- Mode: Lisp -*-

;;;; fp-exception-tests.lisp
;;;;
;;;; Tests regarding floating point exception handling..
;;;;
;;;; See the file COPYING in the main folder for license and copyright
;;;; information.


(in-package "CL-LIA-TESTS")

(use-package "FIVEAM")

(def-suite fp-exception-handling-tests
           :in cl-lia-test-suite
           :description
           "Tests checking float point numbers basic features.")

(in-suite fp-exception-handling-tests)

;;; #+lispworks
(test supported-notifications
  (is-true  (cl-lia:supported-notification-p :division-by-zero))
  (is-true  (cl-lia:supported-notification-p :inexact))
  (is-true  (cl-lia:supported-notification-p :invalid))
  (is-false (cl-lia:supported-notification-p :absolute-precision-underflow))

  (is-true (cl-lia:all-supported-notifications-p
            :inexact
            :infinitary
            :underflow))

  (is-false (cl-lia:all-supported-notifications-p
             :inexact
             :infinitary
             :absolute-precision-underflow
             :underflow))

  (is-true (cl-lia:some-supported-notifications-p
            :inexact
            :infinitary
            :absolute-precision-underflow
            :underflow))
  )


;;; #+lispworks
(test checking-and-setting-notifications
  (signals error (cl-lia:fpe-test-notifications :infinitary))
  (signals error (cl-lia:fpe-check-notifications :infinitary))
  )


;;;; end of file -- fp-exception-tests.lisp
