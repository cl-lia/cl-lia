;;;; -*- Mode: Lisp -*-

;;;; float.lisp
;;;;
;;;; Defintions relateing to floating point types.
;;;;
;;;; See the file COPYING for license and copyright information.


(in-package "CL-LIA")


(deftype float-types ()
  "The enumeration of known floating points types."
  '(member float
           short-float
           single-float
           double-float
           long-float))


;;;; Common setup.
;;;; =============
;;;;
;;;; Floating points "constants" according to LIA 1, 5.2.
;;;; Mosto of them are actually "portable", but it is better to
;;;; dispatch to the implementation.
;;;;
;;;; Notes:
;;;;
;;;; These will probably have to be changed to DEFPARAMETERS, due to
;;;; notorious fascist implementations.

(defconstant rf cl-lia-impl-dep::rf)

(defconstant r-short-float  cl-lia-impl-dep::r-short-float)
(defconstant r-single-float cl-lia-impl-dep::r-single-float)
(defconstant r-double-float cl-lia-impl-dep::r-double-float)
(defconstant r-long-float   cl-lia-impl-dep::r-long-float)


(defconstant p-short-float  cl-lia-impl-dep::p-short-float)
(defconstant p-single-float cl-lia-impl-dep::p-single-float)
(defconstant p-double-float cl-lia-impl-dep::p-double-float)
(defconstant p-long-float   cl-lia-impl-dep::p-long-float)


(defconstant emax-short-float  cl-lia-impl-dep::emax-short-float)
(defconstant emax-single-float cl-lia-impl-dep::emax-short-float)
(defconstant emax-double-float cl-lia-impl-dep::emax-short-float)
(defconstant emax-long-float   cl-lia-impl-dep::emax-short-float)

(defconstant emin-short-float  cl-lia-impl-dep::emin-short-float)
(defconstant emin-single-float cl-lia-impl-dep::emin-single-float)
(defconstant emin-double-float cl-lia-impl-dep::emin-double-float)
(defconstant emin-long-float   cl-lia-impl-dep::emin-long-float)


(defconstant denorm-float-type-p cl-lia-impl-dep:denorm-float-type-p)

(defconstant denorm-short-float-p  cl-lia-impl-dep:denorm-short-float-p)
(defconstant denorm-single-float-p cl-lia-impl-dep:denorm-single-float-p)
(defconstant denorm-double-float-p cl-lia-impl-dep:denorm-double-float-p)
(defconstant denorm-long-float-p   cl-lia-impl-dep:denorm-long-float-p)


(defun display-float-constants (&optional (out *standard-output*))
  (format out "~2&;;; CL-LIA float features for ~A ~A.~%;;;~%"
          (lisp-implementation-type)
          (lisp-implementation-version))

  (format out ";;; rf ~4,4T~S~%" rf)

  (format out ";;; r-short-float ~4,4T~S~%" r-short-float)
  (format out ";;; r-single-float ~4,4T~S~%" r-single-float)
  (format out ";;; r-double-float ~4,4T~S~%" r-double-float)
  (format out ";;; r-long-float ~4,4T~S~%" r-long-float)

  (format out ";;; p-short-float ~4,4T~S~%" p-short-float)
  (format out ";;; p-single-float ~4,4T~S~%" p-single-float)
  (format out ";;; p-double-float ~4,4T~S~%" p-double-float)
  (format out ";;; p-long-float ~4,4T~S~%" p-long-float)

  (format out ";;; emax-short-float ~4,4T~S~%" emax-short-float)
  (format out ";;; emax-single-float ~4,4T~S~%" emax-single-float)
  (format out ";;; emax-double-float ~4,4T~S~%" emax-double-float)
  (format out ";;; emax-long-float ~4,4T~S~%" emax-long-float)

  (format out ";;; emin-short-float ~4,4T~S~%" emin-short-float)
  (format out ";;; emin-single-float ~4,4T~S~%" emin-single-float)
  (format out ";;; emin-double-float ~4,4T~S~%" emin-double-float)
  (format out ";;; emin-long-float ~4,4T~S~%" emin-long-float)

  (format out ";;; denorm-float-type-p ~4,4T~S~%" denorm-float-type-p)

  (format out ";;; denorm-short-float-p ~4,4T~S~%" denorm-short-float-p)
  (format out ";;; denorm-single-float-p ~4,4T~S~%" denorm-single-float-p)
  (format out ";;; denorm-double-float-p ~4,4T~S~%" denorm-double-float-p)
  (format out ";;; denorm-long-float-p ~4,4T~S~%" denorm-long-float-p)
  (terpri out)
  (values)
  )


;;;; API
;;;; ===

(defun make-float (bits &optional (float-type *read-default-float-format*))
  "Builds a floating point number

The function constructs a floating point number of appropriate
float-type, starting from the bit content of bytes.

If bits corresponds to the byte pattern of a NaN, then a NAN is
returned, regardless of float-type.


Arguments and Values:

bits -- An integer or bit-vector representing the binary pattern of a floating point number.
float-type -- A recognizable subtype of float, defaulting to *read-default-float-format*.
result -- the resulting floating point number or NAN.


Exceptional Situations:

The function signals a TYPE-ERROR if either bytes or float-type are
not as described above.


See Also:

NAN.


Notes:

This function is, in one form or another, already present in Common
Lisp implementations.

The interplay between NaNs and make-float is currently underspecified.
"
  (unless (subtypep float-type 'float)
    (error 'type-error
           :datum float-type
           :expected-type '(member float
                                   short-float
                                   single-float
                                   double-float
                                   long-float)))
    
  (cl-lia-impl-dep:make-float bits float-type))


(defun make-short-float (bits)
  (make-float bits 'short-float))

(defun make-single-float (bits)
  (make-float bits 'single-float))

(defun make-double-float (bits)
  (make-float bits 'double-float))

(defun make-long-float (bits)
  (make-float bits 'long-float))


;;; NaNs

(define-symbol-macro s-nan cl-lia-impl-dep:s-nan)

(setf (documentation 's-nan 'variable)
      "A 'signaling not a number' (sNaN).

Notes:

It is to be understood that testing for equality of two NANs is not
meaningful. Especially testing for EQ or EQL.

It is also understood that, (numberp nan) should return T.


See Also:

is-nan, nanp, is-quiet-nan, quiet-nan-p, is-signaling-nan,
signaling-nan-p.
"
      )


(define-symbol-macro q-nan cl-lia-impl-dep:q-nan)

(setf (documentation 'q-nan 'variable)
      "A 'quiet non a number' (qNaN).

Notes:

It is to be understood that testing for equality of two NANs is not
meaningful. Especially testing for EQ or EQL.

It is also understood that, (numberp nan) should return T.


See Also:

is-nan, nanp, is-quiet-nan, quiet-nan-p, is-signaling-nan,
signaling-nan-p.
" 
  )


(define-symbol-macro nan cl-lia-impl-dep:nan)

(setf (documentation 'nan 'variable)
      "A 'not a number' (NaN).

Notes:

It is to be understood that testing for equality of two NANs is not
meaningful. Especially testing for EQ or EQL.

It is also understood that, (numberp nan) should return T.

The value of NAN is always a 'quiet' NaN.


See Also:

is-nan, nanp, is-quiet-nan, quiet-nan-p, is-signaling-nan,
signaling-nan-p.
"
  )


(defun is-nan (x)
  "Returns T whenever the argument X is 'not a number' (NaN)."
  (cl-lia-impl-dep:is-nan x))


(defun nanp (x)
  "Returns T whenever the argument X is 'not a number' (NaN)."
  (is-nan x))


(defun is-quiet-nan (x)
  "Returns T whenever the argument X is a 'quiet' 'not a number' (qNaN)."
  (cl-lia-impl-dep:is-quiet-nan x))


(defun quiet-nan-p (x)
  "Returns T whenever the argument X is a 'quiet' 'not a number' (qNaN)."
  (is-quiet-nan x))


(defun is-signaling-nan (x)
  "Returns T whenever the argument X is a 'signaling' 'not a number' (qNaN)."
  (cl-lia-impl-dep:is-signaling-nan x))


(defun signaling-nan-p (x)
  "Returns T whenever the argument X is a 'signaling' 'not a number' (qNaN)."
  (is-signaling-nan x))


;;; Infinities

#|

  (:export
   "SHORT-FLOAT-NEGATIVE-INFINITY"
   "SHORT-FLOAT-POSITIVE-INFINITY"
   "SINGLE-FLOAT-NEGATIVE-INFINITY"
   "SINGLE-FLOAT-POSITIVE-INFINITY"
   "DOUBLE-FLOAT-NEGATIVE-INFINITY"
   "DOUBLE-FLOAT-POSITIVE-INFINITY"
   "LONG-FLOAT-NEGATIVE-INFINITY"
   "LONG-FLOAT-POSITIVE-INFINITY"
   )

|#

;;; Come back later for doc strings.

(defconstant short-float-negative-infinity
  cl-lia-impl-dep:short-float-negative-infinity)

(defconstant short-float-positive-infinity
  cl-lia-impl-dep:short-float-positive-infinity)

(defconstant single-float-negative-infinity
  cl-lia-impl-dep:single-float-negative-infinity)

(defconstant single-float-positive-infinity
  cl-lia-impl-dep:single-float-positive-infinity)

(defconstant double-float-negative-infinity
  cl-lia-impl-dep:double-float-negative-infinity)

(defconstant double-float-positive-infinity
  cl-lia-impl-dep:double-float-positive-infinity)

(defconstant long-float-negative-infinity
  cl-lia-impl-dep:long-float-negative-infinity)

(defconstant long-float-positive-infinity
  cl-lia-impl-dep:long-float-positive-infinity)


(defconstant infs cl-lia-impl-dep:infs)
(defconstant inff cl-lia-impl-dep:inff)
(defconstant infd cl-lia-impl-dep:infd)
(defconstant infl cl-lia-impl-dep:infl)

(defconstant -infs cl-lia-impl-dep:-infs)
(defconstant -inff cl-lia-impl-dep:-inff)
(defconstant -infd cl-lia-impl-dep:-infd)
(defconstant -infl cl-lia-impl-dep:-infl)


(defun is-infinity (x)
  "The function returns T whenever X is an infinity.

Otherwise it returns NIL.

To clarify, the X must be a representation of an IEEE infinity.
"
  (cl-lia-impl-dep:is-infinity x))


(defun infinityp (x)
  "The function returns T whenever X is an infinity.

Otherwise it returns NIL.

To clarify, the X must be a representation of an IEEE infinity.
"
  (is-infinity x))


(defun is-positive-infinity (x)
  (cl-lia-impl-dep::is-positive-infinity x))


(defun is-negative-infinity (x)
  (cl-lia-impl-dep::is-negative-infinity x))


(defun positive-infinity-p (x)
  (is-positive-infinity x))


(defun negative-infinity-p (x)
  (is-negative-infinity x))


(defun is-finite (x)
  "The function returns T whenever the argument X is not an infinity or a NaN."
  (declare (type real x))
  (and (not (is-nan x)) (not (infinityp x))))


(defun finitep (x)
  "The function returns T whenever the argument X is not an infinity or a NaN."
  (declare (type real x))
  (is-finite x))


;;; Utilities.

(defgeneric float-base-type-< (ft1 ft2)
  (:documentation
   "Checks whether the float type FT2 is 'larger' than float type FT1.

Compound type specifiers are checked against the base type, not taking
into consideration interval constraints.

Exceptional Situations:

An TYPE-ERROR is generated if either FT1 or FT2 is not a member of the
FLOAT-TYPES type.
")
  (:method ((ft1 (eql 'short-float)) (ft2 (eql 'single-float))) t)
  (:method ((ft1 (eql 'short-float)) (ft2 (eql 'double-float))) t)
  (:method ((ft1 (eql 'short-float)) (ft2 (eql 'long-float))) t)

  (:method ((ft1 (eql 'single-float)) (ft2 (eql 'double-float))) t)
  (:method ((ft1 (eql 'single-float)) (ft2 (eql 'long-float))) t)

  (:method ((ft1 (eql 'double-float)) (ft2 (eql 'long-float))) t)

  (:method ((ft1 list) ft2) (float-base-type-< (first ft1) ft2))
  (:method (ft1 (ft2 list)) (float-base-type-< ft1 (first ft2)))

  (:method ((ft1 symbol) (ft2 symbol))
   (cond ((and (typep ft1 'float-types) (typep ft2 'float-types))
          nil)
         ((typep ft1 'float-types)
          (error 'type-error
                 :datum ft2
                 :expected-type 'float-types))
         (t
          (error 'type-error
                 :datum ft1
                 :expected-type 'float-types))
         ))
  )


(defun larger-float-type (ft1 ft2)
  (declare (type symbol ft1 ft2))
  (if (float-base-type-< ft1 ft2)
      ft1
      ft2)
  )


(defun select-infinity-of-type (ft &optional (plusp t))
  (declare (type float-types ft))
  (case ft
    (float (select-infinity-of-type *read-default-float-format*))
    (short-float (if plusp infs -infs))
    (single-float (if plusp inff -inff))
    (double-float (if plusp infd -infd))
    (long-float (if plusp infl -infl))
    (t (error :type-error
              :datum ft
              :expected-type 'float-types))
    ))

;;;; end of file -- float.lisp
