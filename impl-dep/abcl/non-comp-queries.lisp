;;;; -*- Mode: Lisp -*-

;;;; non-comp-queries.lisp
;;;;
;;;; ABCL non computational queries checking the floating point
;;;; environment.
;;;;
;;;; See the file COPYING for license and copyright information.


(in-package "CL-LIA-IMPL-DEP")

#|
 (:export
   "IS-CDR-IEEE-754-CONFORMANT"
   "IS-CDR-IEEE-754-CONSTANTS-PROVIDING"
   "IS-CDR-IEEE-754-ENVIRONMENT-PROVIDING"
   "IS-CDR-IEEE-754-OPERATION-PROVIDING"

   "IS-CL-USING-CDR-IEEE-754"
   )
|#


(defun is-cdr-ieee-754-conformant () nil)


(defun is-cdr-ieee-754-constants-providing () t) ; Well, it is a stretch.


(defun is-cdr-ieee-754-environment-providing () nil)


(defun is-cdr-ieee-754-operation-providing () nil) ; FTTB


(defun is-cl-using-cdr-ieee-754 () nil)


;;;; end of file -- non-comp-queries.lisp
