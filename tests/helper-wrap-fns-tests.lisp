;;;; -*- Mode: Lisp -*-

;;;; helper-wrap-fns-tests.lisp
;;;;
;;;; Tests regarding helper functions.
;;;;
;;;; See the file COPYING in the main folder for license and copyright
;;;; information.


(in-package "CL-LIA-TESTS")

(use-package "FIVEAM")

(def-suite helper-wrap-fns-tests
           :in cl-lia-test-suite
           :description
           "Tests checking non computational (i.e., environmental) features.")

(in-suite helper-wrap-fns-tests)

#+common-lisp ; Just to emphasize.
(test fixnum-integer-result
  (is (cl:= 42 (cl-lia::result-fixnum 42)))
  (let ((i (1+ most-positive-fixnum)))
    (is (cl:= i (cl-lia::result-integer i))))
  )


#+common-lisp ; Just to emphasize.
(test (float-result
       :depends-on (and float-tests rounding-macro-tests rounding-queries))
  (let ((s 42.0s0)
        (f 42.0f0)
        (d 42.0d0)
        (l 42.0l0)
        )
    (is (cl:= (cl-lia::result-f s) s))
    (is (cl:= (cl-lia::result-f f) f))
    (is (cl:= (cl-lia::result-f d) d))
    (is (cl:= (cl-lia::result-f l) l))
    ))


#+common-lisp ; Just to emphasize.
(test (no-result-f-test :depends-on float-tests)
  (is-true (cl-lia:nanp (cl-lia::no-result-f cl-lia:q-nan)))
  (signals error (cl-lia::no-result-f 42.0))
  )


#+common-lisp ; Just to emphasize.
(test (no-result-f-2-test :depends-on float-tests)
  (is-true (cl-lia:nanp (cl-lia::no-result-f-2 42.0 cl-lia:nan)))
  (is-true (cl-lia:nanp (cl-lia::no-result-f-2 cl-lia:nan 42.0)))

  (signals error (cl-lia:nanp (cl-lia::no-result-f-2 42.0 42.0)))
  )

;;;; end of file -- helper-wrap-fns-tests.lisp
