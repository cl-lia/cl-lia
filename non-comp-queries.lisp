;;;; -*- Mode: Lisp -*-

;;;; non-comp-queries.lisp
;;;;
;;;; Non computational queries checking the floating point
;;;; environment.
;;;;
;;;; See the file COPYING for license and copyright information.


(in-package "CL-LIA")

#|
 (:export
   "IS-CDR-IEEE-754-CONFORMANT"
   "IS-CDR-IEEE-754-CONSTANTS-PROVIDING"
   "IS-CDR-IEEE-754-ENVIRONMENT-PROVIDING"
   "IS-CDR-IEEE-754-OPERATION-PROVIDING"

   "IS-CL-USING-CDR-IEEE-754"
   )
|#


(defun is-cdr-ieee-754-conformant ()
  "Return a non-NIL value if the CL-LIA specification is conformant.

If this function returns non-NIL, so will
is-cdr-ieee-754-constants-providing,
is-cdr-ieee-754-environment-providing and
is-cdr-ieee-754-operation-providing.


See Also:

is-cdr-ieee-754-constants-providing,
is-cdr-ieee-754-environment-providing, and
is-cdr-ieee-754-operation-providing.

Notes:

The IEEE Standard for Floating-Point Arithmetic - IEEE Standard 754tm -
2008, 2008 (IEEE Computer Society) specifies two predicates
is754version1985 and is754version2008, but they imply conformance to
the full IEEE-754 standard."

  (cl-lia-impl-dep:is-cdr-ieee-754-conformant))


(defun is-cdr-ieee-754-constants-providing ()
  "Return a non-NIL value if constants naming NaNs and infinities are available."
  (cl-lia-impl-dep:is-cdr-ieee-754-constants-providing))


(defun is-cdr-ieee-754-environment-providing ()
  "Return a non-NIL value if floating point environment is accessible.

This mens that the Common Lisp implementation or the Common Lisp
library provides the operations for accessing and manipulating
floating point exceptions and the floating point environment.
"
  (cl-lia-impl-dep:is-cdr-ieee-754-environment-providing))


(defun is-cdr-ieee-754-operation-providing ()
  "Return a non-NIL value if IEEE 754 operations are available.

This means that the Common Lisp implementation or the Common Lisp
library provides separate implementations of the computational
operations that do conform to the IEEE-754 mandated behavior.
"
  (cl-lia-impl-dep:is-cdr-ieee-754-operation-providing))


(defun is-cl-using-cdr-ieee-754 ()
  "Return a non-NIL value if the mathematical operations respect IEEE 754.

This means tht the mathematical operations in the ANSI Common Lisp
standard respect the IEEE 754 specification.

Notes:

This is a stronger requirement than the presence of the
:ieee-floating-point feature in *features*, as it is a guarantee that
the operations do in fact respect the IEEE 754 specification (and not
only \"purport to conform\").
"
  (cl-lia-impl-dep:is-cl-using-cdr-ieee-754))


;;;; end of file -- non-comp-queries.lisp
