;;;; -*- Mode: Lisp -*-

;;;; predicates.lisp
;;;;
;;;; Predicates on numbers.
;;;;
;;;; It is expected that an implementation will eventually provide
;;;; much lower level definitions.
;;;;
;;;; See the file COPYING for license and copyright information.


(in-package "CL-LIA")


(defgeneric is-zero (n)
  (:documentation "Return T if the argument N is 'zero'.")
  (:method ((n rational)) (cl:zerop n))
  (:method ((n float)) (cl-lia-impl-dep:is-zero n)))


(defgeneric is-neg-zero (n)
  (:documentation "Return T if the argument N is 'zero'.")
  (:method ((n rational)) nil)
  (:method ((n float)) (cl-lia-impl-dep:is-neg-zero n)))


(defun is-tiny (n)
  "Return T if the float N is 'tiny' (or zero)"
  (declare (type float n))
  (if (is-nan n)
      (notify 'floating-point-invalid-operation
              :cv nil
              :operator 'is-tiny
              :operands (list n))
      (cl-lia-impl-dep:is-tiny n)))


(defun tiny-p (n)
  "Return T if the float N is 'tiny' (or zero)"
  (declare (type float n))
  (is-tiny n))

;;;; end of file -- predicates.lisp
