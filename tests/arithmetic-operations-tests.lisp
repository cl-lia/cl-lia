;;;; -*- Mode: Lisp -*-

;;;; arithmetic-operations-tests.lisp
;;;;
;;;; Tests regarding floating point exception handling..
;;;;
;;;; See the file COPYING in the main folder for license and copyright
;;;; information.


(in-package "CL-LIA-TESTS")

(use-package "FIVEAM")

(def-suite arithmetic-operations-tests
           :in cl-lia-test-suite
           :description
           "Tests checking floating point environment support.")

(in-suite arithmetic-operations-tests)

#+common-lisp ; To emphasize
(test (basic-binary-ops-tests
       :depends-on (and fixnum-integer-result
                        float-result
                        no-result-f-test
                        no-result-f-2-test))

  ;; Integers.

  (is (cl:= (cl-lia::_+_ 21 21) 42))
  (is (cl:= (cl-lia::_+_ 21 -21) 0))
  (finishes (cl-lia::_+_ 42 most-positive-fixnum))
  (is-true (cl-lia:is-zero (cl-lia::_+_ 21 -21)))

  (is (cl:= (cl-lia::_-_ 21 21) 0))
  (is (cl:= (cl-lia::_-_ 21 -21) 42))
  (finishes (cl-lia::_-_ most-negative-fixnum 42))
  (is-true (cl-lia:is-zero (cl-lia::_-_ 21 21)))

  (is (cl:= (cl-lia::_*_ 21 2) 42))
  (is (cl:= (cl-lia::_*_ 21 -2) -42))
  (finishes (cl-lia::_*_ most-negative-fixnum 2))
  (is-true (cl-lia:is-zero (cl-lia::_*_ 0 21)))

  (is (cl:= (cl-lia::_/_ 42 21) 2))
  (is (cl:= (cl-lia::_/_ 21 -21) -1))
  (finishes (cl-lia::_/_ most-negative-fixnum 42))
  (is-true (cl-lia:is-zero (cl-lia::_/_ 0 21)))

  (signals cl:division-by-zero (cl-lia::_/_ 42 0))

  ;; Floats.


  ;; Comparisons.

  (is-true  (cl-lia:= 42.0 42.0)) ; Equality on floats?
  (is-false (cl-lia:= pi 3.14))
  (is-true  (cl-lia:= pi pi pi pi))
  (is-false (cl-lia:= cl-lia:inff 3.14))
  (is-false (cl-lia:= 3.14 cl-lia:inff))
  (is-true  (cl-lia:= cl-lia:single-float-positive-infinity cl-lia:inff))
  (is-false (cl-lia:= cl-lia:q-nan 42.0))
  (is-false (cl-lia:= 42.0 cl-lia:q-nan))
  (is-false (cl-lia:= 42.0 42.0 cl-lia:q-nan 42.0))
  (is-false (cl-lia:= cl-lia:q-nan cl-lia:q-nan))
  ;; Missing tests signaling INVALID operation.


  (is-false (cl-lia:/= 42.0 42.0))
  (is-true  (cl-lia:/= pi 3.14))
  (is-false (cl-lia:/= pi pi pi pi))
  (is-true  (cl-lia:/= cl-lia:inff 3.14))
  (is-true  (cl-lia:/= 3.14 cl-lia:inff))
  (is-false (cl-lia:/= cl-lia:single-float-positive-infinity cl-lia:inff))
  (is-true  (cl-lia:/= cl-lia:q-nan 42.0))
  (is-true  (cl-lia:/= 42.0 cl-lia:q-nan))
  (is-true  (cl-lia:/= 42.0 42.1 cl-lia:q-nan 42.3))
  (is-true  (cl-lia:/= cl-lia:q-nan cl-lia:q-nan))
  ;; Missing tests signaling INVALID operation.

  (is-true  (cl-lia:< 42.0 123.0))
  (is-false (cl-lia:< 123.0 42.0))
  (is-false (cl-lia:< 123.0 123.0))
  (is-false (cl-lia:< cl-lia:double-float-negative-infinity
                      cl-lia:double-float-negative-infinity))
  (is-true  (cl-lia:< most-positive-double-float cl-lia:infs))
  (is-true  (cl-lia:< cl-lia:short-float-negative-infinity most-negative-double-float))
  ;; Missing tests signalling INVALID operation.


  (is-true  (cl-lia:<= 42.0 123.0))
  (is-false (cl-lia:<= 123.0 42.0))
  (is-true  (cl-lia:<= 123.0 123.0))
  (is-true  (cl-lia:<= cl-lia:double-float-negative-infinity
                       cl-lia:double-float-negative-infinity))
  (is-true  (cl-lia:<= most-positive-double-float cl-lia:infs))
  (is-true  (cl-lia:<= cl-lia:short-float-negative-infinity most-negative-double-float))
  ;; Missing tests signalling INVALID operation.

 
  ;; Basic operations.

  (is (cl:= (cl-lia::_+_ 21.0 21.0) 42.0)) ; Equality on floats?

  (is-true (cl-lia:is-zero (cl-lia::_+_ 21.0 -21.0)))
  (is-true (cl-lia:is-infinity (cl-lia::_+_ 21.0 cl-lia:infs)))
  (is-true (cl-lia:is-positive-infinity (cl-lia::_+_ 21.0 cl-lia:infs)))
  (is-true (cl-lia:is-negative-infinity (cl-lia::_+_ 21.0 cl-lia:-infd)))
  (is-true (cl-lia:is-negative-infinity (cl-lia::_+_ cl-lia:-inff 21.0)))

  (is-true (and (cl-lia:is-neg-zero -0.0)
                (cl-lia:is-neg-zero (cl-lia::_+_ -0.0 -0.0))))

  (is-true (cl-lia:is-quiet-nan (cl-lia::_+_ cl-lia:q-nan 42.0)))
  (is-true (cl-lia:is-quiet-nan (cl-lia::_+_ 42.0 cl-lia:q-nan)))

  (signals error (cl-lia::_+_ cl-lia:s-nan 42.0))

  (signals error (cl-lia::_+_ cl-lia:infs cl-lia:-infd))
  (signals error (cl-lia::_+_ cl-lia:-infd cl-lia:inff))
  )


;;;; end of file -- arithmetic-operations-tests.lisp
