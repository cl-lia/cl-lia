;;;; -*- Mode: Lisp -*-

;;;; fp-exception-handling.lisp
;;;;
;;;; Lispworks defintions relating to floating points exception handling (cfr., 3.3).
;;;;
;;;; See the file COPYING for license and copyright information.


(in-package "CL-LIA-IMPL-DEP")

#| pre-refactoring
(deftype fpe-notification ()
  "The FPE notifications possibly supported by an implementation."
  ;; This is the same definition of the public interface.
  '(member :division-by-zero :inifinitary
           :inexact
           :invalid
           :underflow
           :overflow
           :absolute-precision-underflow
           ))
|#


(deftype fpe-notification ()
  "The FPE notifications possibly supported by an implementation."
  ;; This is the same definition of the public interface.
  'cl-lia:fpe-notification)


(deftype fpe-notification-set ()
  '(unsigned-byte 8))


;;; *key-selection-alist*
;;; Maybe make these more C-like

(defvar *key-selection-alist*
  '((:division-by-zero .             #b10000000)
    (:infinitary .                   #b10000000)
    (:inexact .                      #b01000000)
    (:invalid .                      #b00100000)
    (:underflow .                    #b00010000)
    (:overflow .                     #b00001000)
    (:absolute-precision-underflow . #b00000100)
    )
  )


(defconstant +fpe-all-notifications+
  #b11111000)


(defun key-selector (fpe-notfn)
  (declare (type fpe-notification fpe-notfn))
  (cdr (assoc fpe-notfn *key-selection-alist* :test #'eq)))


(defun supported-notification-p (fpe-notfn)
  (not (zerop (logand (key-selector fpe-notfn) +fpe-all-notifications+))))


(defun make-notifications-mask (&rest fpe-notifns)
  (apply #'logior (mapcar #'key-selector fpe-notifns)))
  

(defun all-supported-notifications-p (&rest fpe-notifns)
  (let ((fpe-mask (apply #'logior
                         (mapcar #'key-selector fpe-notifns))))
    (declare (type fpe-notification-set fpe-mask))
    (cl:= (logcount fpe-mask)
          (logcount (logand fpe-mask +fpe-all-notifications+)))
    ))


(defun some-supported-notifications-p (&rest fpe-notifns)
  (let ((fpe-mask (apply #'logior
                         (mapcar #'key-selector fpe-notifns))))
    (declare (type fpe-notification-set fpe-mask))
    (cl:plusp (logcount (logior fpe-mask +fpe-all-notifications+)))
    ))


(defun fpe-test-notifications (&rest excps)
  (declare (ignore excps))
  (raise-not-impl 'function-not-implemented
                  :name 'fpe-test-notifications))


(defun fpe-check-notifications (&rest excps)
  (declare (ignore excps))
  (raise-not-impl 'function-not-implemented
                  :name 'fpe-check-notifications))


(defun fpe-clear-notifications (&rest excps)
  (declare (ignore excps))
  (raise-not-impl 'function-not-implemented
                  :name 'fpe-clear-notifications))


;;; Useful inspection functions...

(defun print-fpe-set (&optional
                      (fpe-set +fpe-all-notifications+)
                      (out *standard-output*))
  "Prints the internal representation of FPE-SET on stream OUT."
  (declare (type fpe-notification-set fpe-set))
  (format out "CL-LIA: FPE set is #b~b, #o~:*~o, ~:*~d, #x~:*~x.~%" fpe-set))


;;;; end of file -- fp-exception-handling.lisp
