;;;; -*- Mode: Lisp -*-

;;;; cl-lia-tests-pkg.lisp
;;;;
;;;; The CL-LIA-TESTS package.
;;;;
;;;; See the file COPYING in the main folder for license and copyright
;;;; information.

(eval-when (:load-toplevel :compile-toplevel :execute)
  (unless (find-package "FIVEAM")
    (error "The FIVEAM test framework is required.")))


(defpackage "IT.UNIMIB.DISCO.MA.CL.LIA-TESTS" (:use "CL")
  (:nicknames "CL-LIA-TESTS")
  (:documentation "The CL-LIA-TESTS Package.")


  ;; Shadow lists.
  ;; Should USE the CL-LIA package while being smarter with
  ;; export/import.
  
  )


;;;; end of file -- cl-lia-tests-pkg.lisp
