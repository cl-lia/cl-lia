;;;; -*- Mode: Lisp -*-

;;;; arithmetic-operations.lisp
;;;;
;;;; Lispworks defintions relating to arithmetic operations.
;;;;
;;;; See the file COPYING for license and copyright information.


(in-package "CL-LIA-IMPL-DEP")


(defun ulp (x)
  (declare (type float x))
  ;; Wrong... but FTTB...

  (etypecase x
    (short-float  short-float-epsilon)
    (single-float single-float-epsilon)
    (double-float double-float-epsilon)
    (long-float   long-float-epsilon)
    ))


(defun pred (x)
  (declare (type float x))

  ;; Wrong... but FTTB...
  (cl:- x (ulp x)))


(defun succ (x)
  (declare (type float x))

  ;; Wrong... but FTTB...
  (cl:+ x (ulp x)))


;;;; end of file -- arithmetic-operations.lisp
