;;;; -*- Mode: Lisp -*-

;;;; float-tests.lisp
;;;;
;;;; Tests regarding floating point numbers.
;;;;
;;;; See the file COPYING in the main folder for license and copyright
;;;; information.


(in-package "CL-LIA-TESTS")

(use-package "FIVEAM")

(def-suite float-tests
           :in cl-lia-test-suite
           :description
           "Tests checking float point numbers basic features.")

(in-suite float-tests)

#+lispworks ; Just to emphasize.
(test float-nans
  (is-true (or (boundp 'cl-lia:s-nan)
               (eq :symbol-macro
                   (hcl:variable-information 'cl-lia:s-nan)))
           )
  
  (is-true (or (boundp 'cl-lia:q-nan)
               (eq :symbol-macro
                   (hcl:variable-information 'cl-lia:q-nan)))
           )
  
  (is-true (or (boundp 'cl-lia:nan)
               (eq :symbol-macro
                   (hcl:variable-information 'cl-lia:nan)))
           )

  (is-true (cl-lia:is-nan cl-lia:q-nan))
  (is-true (cl-lia:nanp cl-lia:q-nan))

  (is-true (cl-lia:is-nan cl-lia:nan))

  (signals error cl-lia:s-nan)
  )


#+lispworks
(test zero-predicates
  (is-true (cl-lia:is-zero 0.0))
  (is-true (cl-lia:is-zero (the fixnum 0)))

  (is-false (cl-lia:is-zero 42))
  (is-false (cl-lia:is-zero 42.0))

  (is-false (cl-lia:is-zero cl-lia:long-float-negative-infinity))
  (is-false (cl-lia:is-zero cl-lia:nan))

  (is-false (cl-lia:is-neg-zero 0))
  (is-false (cl-lia:is-neg-zero 0.0))
  (is-false (cl-lia:is-neg-zero -42.0))
  (is-true (cl-lia:is-neg-zero -0.0))
  )


#+common-lisp ; Just to emphasize.
(test float-infinities
  (is-true (boundp 'cl-lia:short-float-negative-infinity))
  (is-true (boundp 'cl-lia:short-float-positive-infinity))
  (is-true (boundp 'cl-lia:single-float-negative-infinity))
  (is-true (boundp 'cl-lia:single-float-positive-infinity))
  (is-true (boundp 'cl-lia:double-float-negative-infinity))
  (is-true (boundp 'cl-lia:double-float-positive-infinity))
  (is-true (boundp 'cl-lia:long-float-negative-infinity))
  (is-true (boundp 'cl-lia:long-float-positive-infinity))

  (is-true (boundp 'cl-lia:infs))
  (is-true (boundp 'cl-lia:-infs))
  (is-true (boundp 'cl-lia:inff))
  (is-true (boundp 'cl-lia:-inff))
  (is-true (boundp 'cl-lia:infd))
  (is-true (boundp 'cl-lia:-infd))
  (is-true (boundp 'cl-lia:infl))
  (is-true (boundp 'cl-lia:-infl))
  )



#+lispworks
(test (float-tests
       :depends-on (and float-nans zero-predicates float-infinities))
  (is-false (cl-lia:is-nan 42))
  (is-false (cl-lia:is-nan cl-lia:double-float-negative-infinity))
  (is-true (cl-lia:is-nan cl-lia:nan))

  (is-true (cl-lia:is-quiet-nan cl-lia:nan))
  ;; (signals error (cl-lia:is-signaling-nan 42))
  (is-false (cl-lia:is-signaling-nan 42))

  (is-false (cl-lia:is-infinity 42))
  (is-false (cl-lia:is-infinity cl-lia:nan))
  (is-true (cl-lia:is-infinity cl-lia:-inff))

  (is-true (cl-lia:is-positive-infinity cl-lia:infs))
  (is-true (cl-lia:is-positive-infinity cl-lia:inff))
  (is-true (cl-lia:is-positive-infinity cl-lia:infd))
  (is-true (cl-lia:is-positive-infinity cl-lia:infl))

  (is-false (cl-lia:is-positive-infinity cl-lia:-infs))
  (is-false (cl-lia:is-positive-infinity cl-lia:-inff))
  (is-false (cl-lia:is-positive-infinity cl-lia:-infd))
  (is-false (cl-lia:is-positive-infinity cl-lia:-infl))

  (is-true (cl-lia:is-negative-infinity cl-lia:-infs))
  (is-true (cl-lia:is-negative-infinity cl-lia:-inff))
  (is-true (cl-lia:is-negative-infinity cl-lia:-infd))
  (is-true (cl-lia:is-negative-infinity cl-lia:-infl))

  (is-false (cl-lia:is-negative-infinity cl-lia:infs))
  (is-false (cl-lia:is-negative-infinity cl-lia:inff))
  (is-false (cl-lia:is-negative-infinity cl-lia:infd))
  (is-false (cl-lia:is-negative-infinity cl-lia:infl))

  (is-true (cl-lia:is-finite 42))
  (is-true (cl-lia:is-finite 0))
  (is-true (cl-lia:is-finite -0))

  (is-false (cl-lia:is-finite cl-lia:double-float-positive-infinity))
  (is-false (cl-lia:is-finite cl-lia:nan))
  
  )


;;;; end of file -- float-tests.lisp
