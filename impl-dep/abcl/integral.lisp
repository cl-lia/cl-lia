;;;; -*- Mode: Lisp -*-

;;;; integral.lisp
;;;;
;;;; ABCL defintions relating to integral types.
;;;;
;;;; See the file COPYING for license and copyright information.


(in-package "CL-LIA-IMPL-DEP")

;;;; This file is just a placeholder as most definitions regarding
;;;; integers and fixnums are implementation independent.


;;;; end of file -- integral.lisp
