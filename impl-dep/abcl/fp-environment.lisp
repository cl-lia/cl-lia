;;;; -*- Mode: Lisp -*-

;;;; fp-environment.lisp
;;;;
;;;; Lispworks defintions relating to floating point environment (cfr., 3.4).
;;;;
;;;; See the file COPYING for license and copyright information.


(in-package "CL-LIA-IMPL-DEP")


(deftype fpe-precision ()
  '(member 19 ; Short floats in LWW 32.
           24
           53
           ;; 64 : not available in Lispworks.
           ))


;;; floating-point-environment
;;;
;;; I know there are way too many indirections, but I believe we want
;;; it so.

(defstruct (floating-point-environment
            (:conc-name %fpe-)
            (:constructor %make-fpe)
            (:copier %copy-fpe) ; Some issues with LWW?
            )
  "The Lispworks very opaque floating point environment object."
  (traps (list :division-by-zero :overflow))
  (rounding-mode :nearest)
  (current-notifications ())
  (accrued-notifications ())
  (precision (float-precision 1.0)) ; I.e., the *read-default-float-format*.
  fast-mode-p
  )


(declaim (type floating-point-environment *default-floating-point-environment*))
(defvar *default-floating-point-environment* (%make-fpe))


(declaim (type floating-point-environment *floating-point-environment*))
(defvar *floating-point-environment*
  (%copy-fpe *default-floating-point-environment*))


(defun fpe-traps (fpe)
  (declare (type floating-point-environment fpe))
  (%fpe-traps fpe))


(defun fpe-rounding-mode (fpe)
  (declare (type floating-point-environment fpe))
  (%fpe-rounding-mode fpe))


(defun fpe-current-notifications (fpe)
  (declare (type floating-point-environment fpe))
  (%fpe-current-notifications fpe))


(defun fpe-accrued-notifications (fpe)
  (declare (type floating-point-environment fpe))
  (%fpe-accrued-notifications fpe))


(defun fpe-precision (fpe)
  (declare (type floating-point-environment fpe))
  (%fpe-precision fpe))


(defun fpe-fast-mode-p (fpe)
  (declare (type floating-point-environment fpe))
  (%fpe-fast-mode-p fpe))


(defun get-floating-point-environment (&aux (fpe *floating-point-environment*))
  (declare (type floating-point-environment fpe))

  (%get-fpe fpe))


(defun %get-fpe (fpe)
  (declare (type floating-point-environment fpe))
  (list :traps (fpe-traps fpe)
        :rounding-mode (fpe-rounding-mode fpe)
        :current-notifications (fpe-current-notifications fpe)
        :accrued-notifications (fpe-accrued-notifications fpe)
        :precision (fpe-precision fpe)
        :fast-mode-p (fpe-fast-mode-p fpe)))


(defun set-floating-point-environment (&key
                                       (traps () traps-s-p)
                                       (rounding-mode :nearest rounding-mode-s-p)
                                       (current-notifications nil current-notifications-s-p)
                                       (precision (float-precision 1.0) precision-s-p)
                                       &allow-other-keys)
  (declare (type boolean
                 traps-s-p
                 rounding-mode-s-p
                 current-notifications-s-p
                 precision-s-p))

  (warn "CL-LIA: Lispworks does not have access to the HW floating ~
         point environment.")
  (when traps-s-p
    (setf (%fpe-traps *floating-point-environment*)
          traps))
  (when rounding-mode-s-p
    (setf (%fpe-rounding-mode *floating-point-environment*)
          rounding-mode))
  (when current-notifications-s-p
    (setf (%fpe-current-notifications *floating-point-environment*)
          current-notifications
          (%fpe-accrued-notifications *floating-point-environment*)
          (delete-duplicates (append current-notifications
                                     (fpe-accrued-notifications
                                      *floating-point-environment*)))
          )
    )
  (when precision-s-p
    (setf (%fpe-precision *floating-point-environment*)
          precision))

  (get-floating-point-environment)
  )

    
(defun default-floating-point-environment ()
  (%get-fpe *default-floating-point-environment*))


(defmacro with-floating-point-environment ((&rest
                                            args
                                            &key
                                            (traps ())
                                            (rounding-mode :nearest)
                                            (current-notifications nil)
                                            (precision
                                             (float-precision 1.0))
                                            )
                                           &body
                                           body)
  "The macro executes BODY in a modified fp environment.

In the modified environment the floating point modes are determined by
the values passed as arguments to the macro. Upon termination (either
normal or exceptional) of the code in body the floating point modes
are restored to those in effect before the execution of
with-floating-point-environment.

As for set-floating-point-environment the values that the arguments
can take are the following:
- traps is a list that can contain the keywords :underflow, :overflow,
:inexact, :invalid, :divide-by-zero, and :denormalized-operand.
- rounding-mode is the rounding mode to use when the result is not
exact; it can assume the values :nearest, :positive-infinity,
:negative-infinity and :zero.
- current-notifications is used to set the exception flags. The main
use is setting the accrued exceptions to NIL to clear them.
- precision can be one of the integers 24, 53 and 64, standing for the
internal precision of the mantissa.

The RESULTS is the value (or values) returned by body.


Notes:

When called with an empty arguments list,
with-floating-point-environment is a no-op and BODY is executed as-is.
"
  (declare (ignore traps rounding-mode current-notifications precision))
  (let ((saved-fp-env-var (gensym "SAVED-FP-ENV-"))
        )
    `(let ((,saved-fp-env-var (get-floating-point-environment)))
       (hcl:unwind-protect-blocking-interrupts-in-cleanups
           (progn (set-floating-point-environment ,@args)
             ,@body)
         (apply #'set-floating-point-environment ,saved-fp-env-var)
         ))
    ))


;;;; end of file -- fp-environment.lisp
