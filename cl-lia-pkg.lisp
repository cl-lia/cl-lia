;;;; -*- Mode: Lisp -*-

;;;; cl-lia-pkg.lisp
;;;;
;;;; The CL-LIA package.
;;;;
;;;; See the file COPYING for license and copyright information.


(defpackage "IT.UNIMIB.DISCO.MA.CL.LIA" (:use "CL")
  (:nicknames "LIA" "CL-LIA" "CL-MATH-LIA-2021")
  (:documentation "The CL-MATH-LIA-2021 Package.

This package (re)exports the symbols that are needed to implement the
`Language Independent Interface'.")

  ;; Shadow lists.

  ;; Basic arithmetic operations.

  (:shadow "=" "/=" "<" "<=" ">" ">=")
  (:shadow "+" "*" "-" "/")

  (:shadow
   "ABS"
   "SIGNUM"
   "SQRT"
   "ISQRT"
   )

  ;; Trigonometric functions.

  (:shadow "SIN" "COS" "TAN" ; ...
   )

  ;; Exponentials and logarithms.

  (:shadow "EXP" "EXPT" "LOG")


  ;; Values dissection.

  (:shadow "SCALE-FLOAT")


  ;; Export lists.

  ;; FIXNUMs and INTEGERs

  (:export
   "BOUNDED-FIXNUM"
   "MINIMUM-FIXNUM"
   "MOST-NEGATIVE-FIXNUM"
   "MAXIMUM-FIXNUM"
   "MOST-POSITIVE-FIXNUM"
   "HAS-INFINITY-FIXNUM" ; This is NIL
   )

  (:export
   "BOUNDED-INTEGER" ; This is NIL
   "HAS-INFINITY-INTEGER" ; This is NIL
   "MINIMUM-INTEGER" ; What should this be?
   "MAXIMUM-INTEGER" ; What should this be?
   )


  ;; FLOATs

  (:export
   "RF"
   "R-SHORT-FLOAT"
   "R-SINGLE-FLOAT"
   "R-DOUBLE-FLOAT"
   "R-LONG-FLOAT"
   "P-SHORT-FLOAT"
   "P-SINGLE-FLOAT"
   "P-DOUBLE-FLOAT"
   "P-LONG-FLOAT"
   "EMAX-SHORT-FLOAT"
   "EMAX-SINGLE-FLOAT"
   "EMAX-DOUBLE-FLOAT"
   "EMAX-LONG-FLOAT"
   "EMIN-SHORT-FLOAT"
   "EMIN-SINGLE-FLOAT"
   "EMIN-DOUBLE-FLOAT"
   "EMIN-LONG-FLOAT"

   "DENORM-FLOAT-TYPE-P"
   "DENORM-SHORT-FLOAT-P"
   "DENORM-SINGLE-FLOAT-P"
   "DENORM-DOUBLE-FLOAT-P"
   "DENORM-LONG-FLOAT-P"
   )

  (:export
   "DISPLAY-FLOAT-CONSTANTS"
   )

  (:export
   "MAKE-FLOAT"
   "MAKE-SHORT-FLOAT"
   "MAKE-SINGLE-FLOAT"
   "MAKE-DOUBLE-FLOAT"
   "MAKE-LONG-FLOAT"
   )

  (:export
   "NAN"
   "S-NAN"
   "Q-NAN"
   )

  (:export
   "IS-NAN"
   "NANP"
   "IS-QUIET-NAN"
   "QUIET-NAN-P"
   "IS-SIGNALING-NAN"
   "SIGNALING-NAN-P"
   )

  (:export
   "SHORT-FLOAT-NEGATIVE-INFINITY"
   "SHORT-FLOAT-POSITIVE-INFINITY"
   "SINGLE-FLOAT-NEGATIVE-INFINITY"
   "SINGLE-FLOAT-POSITIVE-INFINITY"
   "DOUBLE-FLOAT-NEGATIVE-INFINITY"
   "DOUBLE-FLOAT-POSITIVE-INFINITY"
   "LONG-FLOAT-NEGATIVE-INFINITY"
   "LONG-FLOAT-POSITIVE-INFINITY"
   )

  (:export
   "INFS"
   "INFF"
   "INFD"
   "INFL"
   "-INFS"
   "-INFF"
   "-INFD"
   "-INFL"
   )

  (:export
   "IS-INFINITY"
   "INFINITYP"

   "IS-POSITIVE-INFINITY"
   "IS-NEGATIVE-INFINITY"
   "POSITIVE-INFINITY-P"
   "NEGATIVE-INFINITY-P"
   )

  (:export
   "IS-FINITE"
   "FINITEP"
   )

  (:export
   "IS-ZERO"
   "IS-NEG-ZERO"
   )


  ;; FP Notification Style

  (:export
   "NOTIFICATION-STYLE"

   "DYNAMIC-NOTIFICATION-STYLE"
   "DEFAULT-NOTIFICATION-STYLE"
   
   "CURRENT-NOTIFICATION-STYLE"
   "SET-NOTIFICATION-STYLE"
   "WITH-NOTIFICATION-STYLE"

   "NOTIFY"
   )

  ;; FP mirrored conditions

  (:shadow
   "FLOATING-POINT-INEXACT"
   "FLOATING-POINT-INVALID-OPERATION"
   "FLOATING-POINT-OVERFLOW"
   "FLOATING-POINT-UNDERFLOW"
   "DIVISION-BY-ZERO"
   )

  (:export
   "FLOATING-POINT-INEXACT"
   "FLOATING-POINT-INVALID-OPERATION"
   "FLOATING-POINT-OVERFLOW"
   "FLOATING-POINT-UNDERFLOW"
   "DIVISION-BY-ZERO"
   )

  ;; FP Notifications and Environment

  (:export
   "FPE-NOTIFICATION"
   "FPE-NOTIFICATION-SET"
   "+FPE-ALL-NOTIFICATIONS+"
   "SUPPORTED-NOTIFICATION-P"
   "ALL-SUPPORTED-NOTIFICATIONS-P"
   "SOME-SUPPORTED-NOTIFICATIONS-P"
   "FPE-TEST-NOTIFICATIONS"
   "FPE-CHECK-NOTIFICATIONS"
   )

  (:export
   "FLOATING-POINT-ENVIRONMENT"
   
   "FPE-TRAPS"
   "FPE-ROUNDING-MODE"
   "FPE-CURRENT-NOTIFICATIONS"
   "FPE-ACCRUED-NOTIFICATIONS"
   "FPE-PRECISION"
   "FPE-FAST-MODE-P"

   "SET-FLOATING-POINT-ENVIRONMENT"
   "GET-FLOATING-POINT-ENVIRONMENT"

   "DEFAULT-FLOATING-POINT-ENVIRONMENT"
   "RESTORE-DEFAULT-FLOATING-POINT-ENVIRONMENT"

   "WITH-FLOATING-POINT-ENVIRONMENT"
   )

  (:export
   "ROUNDING-MODE"
   "ROUNDING-STYLE"

   "ROUNDING-STYLE-FLOAT"
   "ROUNDING-STYLE-SHORT-FLOAT"
   "ROUNDING-STYLE-SINGLE-FLOAT"
   "ROUNDING-STYLE-DOUBLE-FLOAT"
   "ROUNDING-STYLE-LONG-FLOAT"

   "DEFAULT-ROUNDING-MODE"
   "GET-ROUNDING-MODE"
   "SET-ROUNDING-MODE"

   "WITH-ROUNDING-MODE"

   "ROUND-TO-ZERO"
   "ROUND-TO-NEAR"
   "ROUND-UPWARD"
   "ROUND-DOWNWARD"
   )

  ;; Non computational environment queries

  (:export
   "IS-CDR-IEEE-754-CONFORMANT"
   "IS-CDR-IEEE-754-CONSTANTS-PROVIDING"
   "IS-CDR-IEEE-754-ENVIRONMENT-PROVIDING"
   "IS-CDR-IEEE-754-OPERATION-PROVIDING"

   "IS-CL-USING-CDR-IEEE-754"
   )

  ;; Numeric operations.

  (:export
   "MODULO-FIXNUM"
   "MODULO-INTEGER"
   )

  (:export
   "+" "*" "-" "/"

   "1+" "1-" "INCF" "DECF"

   "CONJUGATE"

   "ABS"
   )

  (:export
   "RESIDUE"
   )

  (:export
   "+.<" "+.>"
   "*.<" "*.>"
   "-.<" "-.>"
   "/.<" "/.>"
   )

  ;; Exponential, Logarithms and Trigonometry Operations.

  (:export
   "ASIN" "ACOS" "ATAN"

   "SIN" "COS" "TAN"

   "ASINH" "ACOSH" "ATANH"

   "SINH" "COSH" "TANH"
   )

  (:export
   "PHASE"

   "EXP" "EXPT" "CIS"
   )

  (:export
   "SQRT"
   "ISQRT"
   )

  (:export
   "SIGNUM"
   "FLOAT-SIGN"
   )

  (:export
   "LOG"
   )

  ;; Numeric Comparison and Predicates.

  (:export
   "/=" "<" "<=" "=" ">=" ">"

   "EVENP" "ODDP"
   "PLUSP" "MINUSP"
   "ZEROP"
   "MIN" "MAX"
   )

  (:export
   "IS-NEG-ZERO"
   "NEG-ZERO-P"
   )

  (:export
   "IS-TINY"
   "TINY-P"
   )

  ;; Value dissection.

  (:export
   "EXPONENT" "FRACTION"
   )

  (:export
   "SCALE-FLOAT"
   )

  (:export
   "SUCC"
   "PRED"
   "ULP"
   )

  (:export
   "INT-PART"
   "FRACT-PART"
   "TRUNCATE-FLOAT"
   "ROUND-FLOAT"
   )
  
  )


;;;; end of file -- cl-lia-pkg.lisp
